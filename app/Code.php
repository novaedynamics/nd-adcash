<?php

namespace App;

use Illuminate\Notifications\Notifiable;
// use Illuminate\Foundation\Auth\User as Authenticatable;
// use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\User;
use Auth;

class Code extends Model
{
    use Notifiable;

    protected $fillable = [
        'code','status','user_id'
    ];

    public function user()
    {
      $this->belongsTo('App\User');
    }

    public function transfers()
    {
        return $this->hasMany('App\Transfer');
    }
    
    protected static function boot()
    {
        parent::boot();

        self::creating(function($model) {
          $model->code = md5(time().rand(00000,99999));
          $model->pin = rand(00000,99999);
          $model->user_id = Auth::user()->id;
        });

        self::updating(function($model) {

        });

    }
}
