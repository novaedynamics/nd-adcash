<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversion extends Model
{
	
	public function items()
	{
		return $this->belongsTo('App\Conversion','parent_id','id');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}

}