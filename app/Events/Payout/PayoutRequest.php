<?php

namespace App\Events\Payout;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Http\Request;

use App\User;

class PayoutRequest
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $attr;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, $attr = array())
    {
        $this->user = $user;
        $this->attr = $attr;
    }

}
