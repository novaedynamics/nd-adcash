<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\User;
use App\Tree;
use App\Code;
use App\Pin;

class UpgradeAccount
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $tree;
    public $code;
    public $pin;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    public function __construct(User $user, Tree $tree, Code $code, Pin $pin)
    {

        $this->user = $user;
        $this->tree = $tree;
        $this->code = $code;
        $this->pin = $pin;
    }

}
