<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Role;
use Cookie;
use App\Events\UserRegistered;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Config;
use Log;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/affiliates';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        if(!$this->bool_sponsor(Cookie::get('sponsor_id'))) return $this->referral();
        return view('auth.register',['sponsor' => $this->get_sponsor()]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        $avatar = 'avatars/'.rand(01,22).'.png';

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'parent_id' => $this->get_sponsor()->id,
            'password' => bcrypt($data['password']),
            'avatar' => $avatar
        ]);

        $user->roles()->attach(Role::where('name','Regular')->first());

        event(new UserRegistered($user));

        return $user;

    }

    protected function get_sponsor()
    {   
        return User::where('id',Cookie::get('sponsor_id'))->first();
    }

    protected function bool_sponsor($sponsor_id)
    {
        $count = User::where('id',$sponsor_id)->count();
        if($count > 0)
            return true;
        return false;
    }

    public function referral($referral = null) 
    { 
        if(Cookie::get('referral_id') == null || Cookie::get('referral_id') == "") $cookie = $this->default_referral();
        if(!$this->bool_referral($referral)) $cookie = $this->default_referral();
        if($this->bool_referral($referral)) $cookie = $this->get_referral($referral);
        return redirect()->route('register')->withCookie($cookie);
    }

    protected function default_referral()
    {
        if($this->bool_referral(config('adcash.referral.default'))) $referral = config('adcash.referral.default');
        if(!$this->bool_referral(config('adcash.referral.default'))) $referral = User::first();
        return cookie('sponsor_id', $referral->id, 43200); // 30 days
    }

    protected function get_referral($referral)
    {
        $sponsor = User::where('referral_id',$referral)->first();
        return cookie('sponsor_id', $sponsor->id, 43200);
    }

    protected function bool_referral($referral_id)
    {
        $bool = User::where('referral_id',$referral_id)->count();
        if($bool)
            return true;
        return false;
    }

}
