<?php

namespace App\Http\Controllers;

use App\Events\EventConversion;
use Illuminate\Http\Request;
use App\Conversion;
use App\Code;
use App\Pin;
use App\User;
use App\Transfer;
use Auth;
use App\Wallet;
use Config;
use Log;

use App\Events\CodeTransfer;


class CodesController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}

    public function index()
    {
    	$codes = Auth::user()->codes()->where('status','active')->get();
        $transfers = Auth::user()->transfers()->where('type','code')->with(['user' => function($q) {
            return $q->select('id','name','lastname');
        }, 'code' => function($q) {
            return $q;
        }])->get();

    	return view('codes.index',[
            'codes' => $codes,
            'transfers' => $transfers
            ]);
    }

    public function create()
    {
        $credit = Wallet::where('user_id',Auth::user()->id)->where('action','credit')->sum('amount');
        $debit = Wallet::where('user_id',Auth::user()->id)->where('action','debit')->sum('amount');
        $balance = $credit - $debit;
        
        // $quantity = 2;
        $quantity = floor($balance / (int)config('adcash.activation-code.price'));

    	return view('codes.purchase',['quantity' => $quantity]);
    }

    public function post(Request $request)
    {
        // Check Wallet if able to purchase

        $credit = Wallet::where('user_id',Auth::user()->id)->where('action','credit')->sum('amount');
        $debit = Wallet::where('user_id',Auth::user()->id)->where('action','debit')->sum('amount');
        $balance = $credit - $debit;

        $quantity = floor($balance / (int)config('adcash.activation-code.price'));        

        if($request->get('quantity') < config('adcash.activation-code.minimum')) {
            $request->session()->flash('danger','Oops! A minimum of 2 codes purchase is required.');
            return redirect()->route('purchase-codes');
        }

        if($request->get('quantity') <= $quantity) {

            $generated = array();
            $limit = $request->get('quantity');

            for($i = 0; $i < $request->get('quantity'); $i++) {
                $code = new Code();
                $code->save();
                $this->debit_account();
                array_push($generated,[
                    'code' => $code->code,
                    'id' => $code->id]);
            }

            $attr = array(
                'title' => "Generate $limit codes",
                'action' => 'generate',
                'type'  => 'code',
                'items' => $generated);

            event(new EventConversion(Auth::user(),$attr));

            $request->session()->flash('success','Congratulations! Your purchased is successful.');

            return redirect()->route('codes');
        }

        return redirect()->route('codes');       

    }

    public function transfer($code = null)
    {
        $count_codes = Auth::user()->codes()->where('status','active')->count();
        $transfers = Auth::user()->transfers()->where('type','code')->get();
        return view('codes.transfer',[
            'count_codes' => $count_codes,
            'transfers' => $transfers
            ]);
    }

    public function generate(Request $request)
    {
        if($request->isMethod('POST'))
        {
            $role = Auth::user()->roles()->first()->name;
            $limit = $request->get('quantity');
            if(!in_array($role,['Admin','Super admin'])) return redirect()->route('codes');
            if( $limit == null || $limit < 1 ) return redirect()->route('codes');

            $generated = array();

            for($i = 0; $i < $limit; $i++) {
                $code = new Code();
                $code->save();
                array_push($generated,[
                    'code' => $code->code,
                    'id' => $code->id]);
            }

            $attr = array(
                'title' => "Generate $limit codes",
                'action' => 'generate',
                'type'  => 'code',
                'items' => $generated);

            event(new EventConversion(Auth::user(),$attr));

            return redirect()->route('codes');

        }
        return view('codes.generate');
    }

    public function post_transfer(Request $request)
    {
        $code_count =  Auth::user()->codes()->where('status','active')->count();
        if($request->isMethod('POST')) {

            if( $code_count == 0 ) return back()->with('danger','No PIN available!');
            if( $request->get('quantity') < 1 ) return back()->with('danger','Invalid PIN quantity!');
            if( $request->get('email') == Auth::user()->email ) return back()->with('danger','You cannot set your email as recipient!');
            if( !$this->bool_email($request->get('email')) ) return back()->with('danger','Invalid email recipient!');
            
            $user = User::where('email',$request->get('email'))->first();
            $codes = Auth::user()->codes()->where('status','active')->take($request->get('quantity'))->get();
            $from = Auth::user()->id;

            $generated = array();

            foreach($codes as $c):
                
                $code = Code::find($c->id);
                $code->user_id = $user->id;
                $code->save();

                $this->create_transfer(array(
                    'user' => $user->id,
                    'from' => $from,
                    'code' => $code->id,
                    'message' => 'You just received a Code from ' . Auth::user()->name .' '. Auth::user()->lastname,
                    'type' => 'code'));
                $this->create_transfer(array(
                    'user' => Auth::user()->id,
                    'from' => null,
                    'code' => $code->id,
                    'message' => 'You just transfered a Code to ' . $user->name .' '. $user->lastname,
                    'type' => 'code'));
                
                array_push($generated,[
                    'code' => $code->code,
                    'id' => $code->id]);

            endforeach;

            $name = $user->name . ' ' . $user->lastname;

            $request->session()->flash('name',$name);
            $request->session()->flash('success',true);

            $limit = $request->get('quantity');

            $attr = array(
                'title' => "Transfer $limit codes to $user->name $user->lastname : $user->email",
                'action' => 'transfer',
                'type'  => 'code',
                'items' => $generated);

            event(new EventConversion(Auth::user(),$attr));


            return redirect()
                    ->route('transfer-codes');
        }
    }

    protected function debit_account()
    {
        $wallet = new Wallet();
        $wallet->amount = config('adcash.activation-code.price');
        $wallet->action = 'debit';
        $wallet->type = 'payout';
        $wallet->status = 'C';
        $wallet->description = config('adcash.description.bonus.payout-code');
        $wallet->user_id = Auth::user()->id;
        $wallet->ip = \Request::ip();
        $wallet->save();
    }

    protected function bool_email($email)
    {
        $bool = User::where('email',$email)->count();
        if($bool > 0)
            return true;
        return false;
    }

    protected function create_transfer($attr = array())
    {
        $transfer = new Transfer();
        $transfer->from_id = $attr['from'];
        $transfer->user_id = $attr['user'];
        $transfer->code_id = $attr['code'];
        $transfer->description = $attr['message'];
        $transfer->type = $attr['type'];
        $transfer->ip = \Request::ip();
        $transfer->save();
    }

}
