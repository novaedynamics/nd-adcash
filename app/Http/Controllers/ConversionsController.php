<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Carbon\Carbon;
use App\User;
use App\Conversion;


class ConversionsController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(Request $request)
	{
		$tz = 'Asia/Manila';

		$from = Carbon::createFromDate(null,2,1,$tz);
		$end = Carbon::createFromDate(null,8,1,$tz);

		if(!empty($request->get('from'))) $from = Carbon::createFromDate($request->get('from'));
		if(!empty($request->get('end')))  $end  = Carbon::createFromDate($request->get('end'));

		// return Conversion::select('user_id',DB::raw('COUNT(*) as total'))
		// 		->whereBetween('created_at', [$from,$end])
  //               ->with('user')
  //               ->with('type','code')
  //               ->with('action','generate')
  //               ->withNotNull('parent_id')
  //               ->groupBy('user_id')
  //               ->orderBy('total', 'DESC')
  //               ->take(10)
  //               ->get();
        return Conversion::select('user_id',DB::raw('COUNT(*) as total'))
				->whereBetween('created_at', [$from,$end])
                // ->with('user')
                // ->with('type','code')
                // ->with('action','generate')
                // ->withNotNull('parent_id')
                ->groupBy('user_id')
                ->orderBy('total', 'DESC')
                ->take(10)
                ->get();
	}

}
