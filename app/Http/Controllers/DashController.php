<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Wallet;
use App\User;
use Auth;
use Carbon\Carbon;
use App\Conversion;

class DashController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $wallet = $this->wallet();
        $wallet_debit = $this->wallet_debit();
        $wallet_credit = $this->wallet_credit();
        $wallet_pending_payout = $this->wallet_pending_payout();
        $wallet_payout = $this->wallet_payout();
        $wallet_total = $wallet_credit - $wallet_debit;

        $recent_users = $this->recent_users(3);
        $member_activities = User::has('wallets')->inRandomOrder()->with('directs')->take(6)->get();

        $now = Carbon::now();
        $users_new = User::where('created_at','>=',$now->startOfWeek())
                         ->count();
        $users_count = User::count();
        $user_finance = $this->get_finance();

        // User Count
        $count_active_users_today = $this->count_active_users('today');
        $count_active_users_week = $this->count_active_users('week');
        $count_active_users_month = $this->count_active_users('month');

        // Tops
        $top_today              = $this->top_today('DESC',10)->get();
        $top_week               = $this->top_week('DESC',10)->get();
        $top_month              = $this->top_month('DESC',10)->get();

        $top_today_total        = $this->top_today_total('DESC',10);
        $top_week_total         = $this->top_week_total('DESC',10);
        $top_month_total        = $this->top_month_total('DESC',10);

        $top_converter_day      =   $this->topConverterDay($request);
        $top_converter_week     =   $this->topConverterWeek($request);
        $top_converter_month    =   $this->topConverterMonth($request); 
        $top_converter_range    =   $this->topConverterRange($request);

        return view('dash.index',[
            'wallet_total'          => $wallet_total,
            'wallet_bonus'          => $wallet_credit,
            'wallet_payout'         => $wallet_payout,
            'wallet_pending_payout' => $wallet_pending_payout,
            'wallet'                => $wallet,
            'recent_users'          => $recent_users,
            'member_activities'     => $member_activities,
            'users_count'           => $users_count,
            'users_new'             => $users_new,
            'user_finance'          => $user_finance,
            'top_today'             => $top_today,
            'top_week'              => $top_week,
            'top_month'             => $top_month,
            'top_today_total'       => $top_today_total,
            'top_week_total'        => $top_week_total,
            'top_week_total'        => $top_week_total,
            'top_converter_day'     => $top_converter_day,
            'top_converter_week'    => $top_converter_week,
            'top_converter_month'       => $top_converter_month,
            'top_converter_range'       => $top_converter_range,
            'count_active_users_today'  => $count_active_users_today,
            'count_active_users_week'   => $count_active_users_week,
            'count_active_users_month'  => $count_active_users_month,
            ]);
    }

    protected function wallet()
    {
        return Wallet::where('user_id',Auth::user()->id)->get();
    }

    protected function wallet_total()
    {
        return $this->wallet_credit() - $this->wallet_debit();
    }

    protected function wallet_debit()
    {
        return Wallet::where('user_id',Auth::user()->id)->where('action','debit')->sum('amount');
    }

    protected function wallet_credit()
    {
        return Wallet::where('user_id',Auth::user()->id)->where('action','credit')->sum('amount');
    }

    protected function wallet_pending_payout()
    {
        return Wallet::where('user_id',Auth::user()->id)
                                    ->where('action','debit')
                                    ->where('type','payout')
                                    ->where('status','!=','C')
                                    ->sum('amount');
    }

    protected function wallet_payout()
    {
        return Wallet::where('user_id',Auth::user()->id)
                                    ->where('action','debit')
                                    ->where('type','payout')
                                    ->where('status','C')
                                    ->sum('amount');
    }

    public function recent_users($number)
    {
        return User::inRandomOrder()->take($number)->get();
    }    

    protected function get_finance()
    {
        $income = Wallet::where('user_id',Auth::user()->id)
                            ->get()
                            ->groupBy(function($field) {
                                    return Carbon::parse($field->created_at)->format('D');
                                });

        $report = array();

        foreach ($income as $i) {
                $report[] = array(
                    'day' => $i->first()->created_at->format('d'),
                    'payout' => $i->where('action','debit')->sum('amount'),
                    'earnings' => $i->sum('amount'));
        }   

        return $report;

    }

    protected function top_today($sort,$limit)
    {
        return Wallet::select('user_id',DB::raw('SUM(amount) as earnings'))
                ->where('action','credit')
                ->whereDate('created_at', Carbon::today())
                ->whereHas('user.roles', function($q) {
                    $q->where('name','Premium');
                })
                ->whereHas('user', function($q) {
                    $q->whereNotIn('email', $this->ignoreAccounts());
                })
                ->groupBy('user_id')
                ->orderBy('earnings', $sort)
                ->take($limit);
    }

    protected function top_week($sort,$limit)
    {
        return Wallet::select('user_id',DB::raw('SUM(amount) as earnings'))
                ->where('action','credit')
                ->whereBetween('created_at', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])
                ->whereHas('user.roles', function($q) {
                    $q->where('name','Premium');
                })
                ->whereHas('user', function($q) {
                    $q->whereNotIn('email', $this->ignoreAccounts());
                })
                ->groupBy('user_id')
                ->orderBy('earnings', $sort)
                ->take($limit);
    }

    protected function top_month($sort,$limit)
    {
        return Wallet::select('user_id',DB::raw('SUM(amount) as earnings'))
                ->where('action','credit')
                ->whereBetween('created_at', [Carbon::now()->startOfMonth(),Carbon::now()->endOfMonth()])
                ->whereHas('user.roles', function($q) {
                    $q->where('name','Premium');
                })
                ->whereHas('user', function($q) {
                    $q->whereNotIn('email', $this->ignoreAccounts());
                })
                ->groupBy('user_id')
                ->orderBy('earnings', $sort)
                ->take($limit);
    }

    protected function top_today_total()
    {
        return Wallet::whereDate('created_at', Carbon::today())->count();
    }

    protected function top_week_total()
    {
        return Wallet::whereBetween('created_at', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])->count();
    }

    protected function top_month_total()
    {
        return Wallet::whereBetween('created_at', [Carbon::now()->startOfMonth(),Carbon::now()->endOfMonth()])->count();
    }

    public function topConverterDay(Request $request)
    {
        $tz = 'Asia/Manila';

        $today = Carbon::today();

        return Conversion::select('user_id',DB::raw('COUNT(*) as total'))
                ->where('created_at', $today)
                ->where('type','code')
                ->where('action','generate')
                ->whereNotNull('parent_id')
                ->whereHas('user.roles', function($q) {
                    $q->where('name', 'Premium');
                })
                ->groupBy('user_id')
                ->orderBy('total', 'DESC')
                ->take(10)
                ->get();
    }

    public function topConverterWeek(Request $request)
    {
        $tz = 'Asia/Manila';

        $from = Carbon::now()->startOfWeek();
        $end = Carbon::now()->endOfWeek();

        if(!empty($request->get('from'))) $from = Carbon::createFromDate($request->get('from'));
        if(!empty($request->get('end')))  $end  = Carbon::createFromDate($request->get('end'));

        return Conversion::select('user_id',DB::raw('COUNT(*) as total'))
                ->whereBetween('created_at', [$from,$end])
                ->where('type','code')
                ->where('action','generate')
                ->whereNotNull('parent_id')
                ->whereHas('user.roles', function($q) {
                    $q->where('name','Premium');
                })
                ->whereHas('user', function($q) {
                    $q->whereNotIn('email', $this->ignoreAccounts());
                })
                ->groupBy('user_id')
                ->orderBy('total', 'DESC')
                ->take(10)
                ->get();
    }

    public function topConverterMonth(Request $request)
    {
        $tz = 'Asia/Manila';

        $from = Carbon::now()->startOfMonth();
        $end = Carbon::now()->endOfMonth();

        if(!empty($request->get('from'))) $from = Carbon::createFromDate($request->get('from'));
        if(!empty($request->get('end')))  $end  = Carbon::createFromDate($request->get('end'));

        return Conversion::select('user_id',DB::raw('COUNT(*) as total'))
                ->whereBetween('created_at', [$from,$end])
                ->where('type','code')
                ->where('action','generate')
                ->whereNotNull('parent_id')
                ->whereHas('user.roles', function($q) {
                    $q->where('name','Premium');
                })
                ->whereHas('user', function($q) {
                    $q->whereNotIn('email', $this->ignoreAccounts());
                })
                ->groupBy('user_id')
                ->orderBy('total', 'DESC')
                ->take(10)
                ->get();
    }

    public function topConverterRange(Request $request)
    {
        $tz = 'Asia/Manila';

        $from = Carbon::createFromDate(null,2,1,$tz);
        $end = Carbon::createFromDate(null,8,1,$tz);

        if(!empty($request->get('from'))) $from = Carbon::createFromDate($request->get('from'));
        if(!empty($request->get('end')))  $end  = Carbon::createFromDate($request->get('end'));

        return Conversion::select('user_id',DB::raw('COUNT(*) as total'))
                ->whereBetween('created_at', [$from,$end])
                ->where('type','code')
                ->where('action','generate')
                ->whereNotNull('parent_id')
                ->whereHas('user.roles', function($q) {
                    $q->where('name','Premium');
                })
                ->whereHas('user', function($q) {
                    $q->whereNotIn('email', $this->ignoreAccounts());
                })
                ->groupBy('user_id')
                ->orderBy('total', 'DESC')
                ->take(10)
                ->get();
    }

    protected function count_active_users($option)
    {
        if($option == 'today') {
            $count  =   User::whereHas('roles', function($q) {
                            $q->where('name','Premium');
                        })
                        ->whereDate('created_at',Carbon::today())
                        ->whereNotIn('email',['premium@adcash.network'])
                        ->count();
        }
        if($option == 'week') {
            $count  =   User::whereHas('roles', function($q) {
                            $q->where('name','Premium');
                        })
                        ->whereBetween('created_at', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])
                        ->whereNotIn('email',['premium@adcash.network'])
                        ->count();
        }
        if($option == 'month') {
            $count  =   User::whereHas('roles', function($q) {
                            $q->where('name','Premium');
                        })
                        ->whereBetween('created_at',[Carbon::now()->startOfMonth(),Carbon::now()->endOfMonth()])
                        ->whereNotIn('email',['premium@adcash.network'])
                        ->count();
        }

        return $count;
    }

    protected function ignoreAccounts()
    {
        return ['admin@adcash.network',
                'bradie.jhun@adcash.network',
                'trulyrichjhun@gmail.com',
                'adcash01@adcash.network',
                'adcash02@adcash.network',
                'adcash03@adcash.network',
                'adcash04@adcash.network',
                'adcash05@adcash.network',
                'adcash06@adcash.network',
                'adcash07@adcash.network'];
    }

}
