<?php

namespace App\Http\Controllers;

use App\Events\Payout\PayoutRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Wallet;
use App\Payout;
use Auth;
use Log;
use App\User;
use Carbon\Carbon;
use App\Role;
use Config;

class FinancesController extends Controller
{
    
	public function __construct()
	{
		$this->middleware('auth');
	}

    public function index()
    {
        $wallet = $this->wallet();
        $wallet_debit = $this->wallet_debit();
        $wallet_credit = $this->wallet_credit();
        $wallet_pending_payout = $this->wallet_pending_payout();
        $wallet_payout = $this->wallet_payout();
        $wallet_total = $wallet_credit - $wallet_debit;

    	return view('finance.index',[
            'wallet_total' => $wallet_total,
            'wallet_bonus' => $wallet_credit,
            'wallet_payout' => $wallet_payout,
            'wallet_pending_payout' => $wallet_pending_payout,
            'wallet' => $wallet]);
    }

    public function withdraw_history()
    {
        $payouts = Auth::user()->payouts()->orderBy('created_at','DESC')->with('user')->get();
        return view('withdraw.history', ['payouts' => $payouts]);
    }

    public function withdraw()
    {

        $pending_withdrawal     = $this->bool_week_pending_withdrawals();

        $wallet                 = $this->wallet();
        $wallet_debit           = $this->wallet_debit();
        $wallet_credit          = $this->wallet_credit();
        $wallet_pending_payout  = $this->wallet_pending_payout();
        $wallet_payout          = $this->wallet_payout();
        $wallet_total           = $this->wallet_total();

        $method         =   config('adcash.withdrawal.method');
        $denomination   =   config('adcash.withdrawal.denomination');
        $currency       =   config('adcash.currency.us');

        $cutoff_date = $this->cutoff_date();

    	return view('finance.withdraw',[
            'wallet_total' => $wallet_total,
            'wallet_bonus' => $wallet_credit,
            'wallet_payout' => $wallet_payout,
            'wallet_pending_payout' => $wallet_pending_payout,
            'wallet' => $wallet,
            'denomination' => $denomination,
            'currency' => $currency,
            'method' => $method,
            'cutoff_date' => $cutoff_date,
            'pending_withdrawal' => $pending_withdrawal
            ]);

    }

    public function request_payout(Request $request)
    {

        $validator = Validator::make($request->all(),[
                        'amount' => 'required',
                        'method' => 'required'
                    ]);

        if(!$this->bool_first_payout() && $request->get('amount') < 250) return response()->json([
            'success' => false, 'message' => '250'],200);
        if($validator->fails()) return response()->json([
            'success' => false,'pending' => false],200);
        if(!$this->bool_allow_payout($request->get('amount'))) return response()->json([
            'success' => false,'pending' => false],200);

        // Check if atleast 1 pending withdrawal
        if($this->bool_week_pending_withdrawals()) return response()->json(['success' => false,'pending' => true],200);

        $this->create_payout_record($request);
        return response()->json(['success' => true],200);
    }

    public function request_payouts(Request $request)
    {
        return view('finance.requests');
    }

    public function all_pending_payouts()
    {
        $data = Payout::with('user')->orderby('created_at','DESC')->get()->toArray();
        return response()->json(['data' => $data],200);
    }

    protected function wallet()
    {
        return Wallet::where('user_id',Auth::user()->id)->orderBy('id','DESC')->get();
    }

    protected function wallet_total()
    {
        return $this->wallet_credit() - $this->wallet_debit();
    }

    protected function wallet_debit()
    {
        return Wallet::where('user_id',Auth::user()->id)->where('action','debit')->sum('amount');
    }

    protected function wallet_credit()
    {
        return Wallet::where('user_id',Auth::user()->id)->where('action','credit')->sum('amount');
    }

    protected function wallet_pending_payout()
    {
        return Wallet::where('user_id',Auth::user()->id)
                                    ->where('action','debit')
                                    ->where('type','payout')
                                    ->where('status','!=','C')
                                    ->sum('amount');
    }

    protected function wallet_payout()
    {
        return Wallet::where('user_id',Auth::user()->id)
                                    ->where('action','debit')
                                    ->where('type','payout')
                                    ->where('status','C')
                                    ->sum('amount');
    }
    
    protected function create_payout_record(Request $request)
    {
        $userid = $request->get('userid');
        $method = $request->get('method');
        $amount = $request->get('amount');
        $waddress = $request->get('waddress');
        $wmessage = $request->get('message');
        $wmobile = $request->get('wmobile');
        $name = Auth::user()->name .' '. Auth::user()->lastname;

        $payout = new Payout();
        $payout->method = $method;
        $payout->wallet = $waddress;
        $payout->amount = $amount;
        $payout->message = $wmessage;
        $payout->mobile = $wmobile;
        $payout->status = 'processing';
        $payout->user_id = Auth::user()->id;
        $payout->save();

        event(new PayoutRequest(User::find(Auth::user()->id), $request));

    }

    // check for payout
    protected function bool_allow_payout($amount)
    {
        if($this->wallet_total()  < $amount) return false;
        if(!in_array($amount,config('adcash.withdrawal.denomination'))) return false;
        return true;
    }

    protected function bool_pending_withdrawal()
    {
        // Check if if it has pending withdrawal
        $count = Payout::where('user_id',Auth::user()->id)->count();
        if($count > 0) return true;
        return false;
    }

    protected function get_finance()
    {
        $income = Wallet::where('user_id',Auth::user()->id)
                            ->get()
                            ->groupBy(function($field) {
                                    return Carbon::parse($field->created_at)->format('D');
                                });

        $report = array();

        foreach ($income as $i) {
                $report[] = array(
                    'date' => $i->first()->created_at->format('d M'),
                    'payout' => $i->where('action','debit')->sum('amount'),
                    'earnings' => $i->sum('amount'));
        }   

        return $report;

    }


    public function get_signups()
    {
        $children = User::with('roles')
                    ->where('parent_id',Auth::user()->id)
                    ->get()
                    ->groupBy(function($field) {
                            return Carbon::parse($field->created_at)->format('Y-m-d');
                        });

        $report = array();

        foreach($children as $c):
            
            $active = 0;
            $inactive = 0;
            
            foreach ($c as $child) {
                if(in_array($child->roles()->first()->name,['Premium'])) {
                    $active++;
                } else {
                    $inactive++;
                }
            }         

            $report[] = array(
                    'date' => $c->first()->created_at->format('Y-m-d'),
                    'signups' => $c->count(),
                    'active' => $active,
                    'inactive' => $inactive);

        endforeach;

        return $report;
    }

    protected function cutoff_date()
    {
        $date = Carbon::parse('next thursdays')->format('l jS \\of F Y');
        if(date('D',Carbon::today()->timestamp) == 'Thu') $date = Carbon::today()->format('l jS \\of F Y');
        return $date;
    }

    protected function bool_week_pending_withdrawals()
    {
        $count = Payout::where('user_id',Auth::user()->id)
                ->whereBetween('created_at',[Carbon::parse('last Friday'),Carbon::parse('this Thursday')])
                ->where('status','processing')
                ->count();
        if($count > 0) return true;
        return false;
    }

    protected function bool_first_payout()
    {
        $count = Payout::where('user_id',Auth::user()->id)
                ->where('status','success')
                ->count();
        if($count > 0) return true;
        return false;
    }

}
