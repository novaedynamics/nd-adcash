<?php

namespace App\Http\Controllers;

use App\Events\EventConversion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Pin;
use App\User;
use App\Transfer;
use Auth;

class PinsController extends Controller
{
    public function __construct() 
    {

    }

    public function index()
    {
        $pins = Auth::user()->pins()->where('status','active')->get();
        $transfers = Auth::user()->transfers()->where('type','pin')->get();
    	return view('pin.index',array(
            'pins' => $pins,
            'transfers' => $transfers
        ));
    }

    public function transfer(Request $request)
    {
        $pin_count =  Auth::user()->pins()->count();
        return view('pin.transfer', array(
            'pins' => $pin_count
        ));
    }

    public function action_transfer(Request $request)
    {
        $pin_count =  Auth::user()->pins()->count();
        if($request->isMethod('POST')) {

            if( $pin_count == 0 ) return back()->with('danger','No PIN available!');
            if( $request->get('quantity') < 1 ) return back()->with('danger','Invalid PIN quantity!');
            if( $request->get('email') == Auth::user()->email ) return back()->with('danger','You cannot set your email as recipient!');
            if( !$this->bool_email($request->get('email')) ) return back()->with('danger','Email is not in our system!');
            
            $user = User::where('email',$request->get('email'))->first();
            $pins = Auth::user()->pins()->where('status','active')->take($request->get('quantity'))->get();
            $from = Auth::user()->id;

            $name = $user->name.' '.$user->lastname;

            $request->session()->flash('success',$name);

            $generated = array();

            foreach($pins as $p):
                
                $pin = Pin::find($p->id);
                $pin->user_id = $user->id;
                $pin->save();

                $this->create_transfer(array(
                    'user' => $user->id,
                    'from' => $from,
                    'code' => $pin->id,
                    'message' => 'You just received a PIN from ' . Auth::user()->name .' '. Auth::user()->lastname,
                    'type' => 'pin'));
                $this->create_transfer(array(
                    'user' => Auth::user()->id,
                    'from' => null,
                    'code' => $pin->id,
                    'message' => 'You just transfered a PIN to ' . $user->name .' '. $user->lastname,
                    'type' => 'pin'));
                array_push($generated,[
                    'pin' => $pin->pin,
                    'id' => $pin->id]);
            endforeach;

            $limit = $request->get('quantity');

            $attr = array(
                'title' => "Transfer $limit pins to $user->name $user->lastname : $user->email",
                'action' => 'transfer',
                'type'  => 'pin',
                'items' => $generated);

            event(new EventConversion(Auth::user(),$attr));

            return redirect()                   
                    ->back();
        }

    }

    protected function get_user_pin($user_id)
    {
        return Ping::where('user_id',$user_id)->get();
    }

    public function generate(Request $request)
    {        
        if($request->isMethod('POST'))
        {
            $role = Auth::user()->roles()->first()->name;
            $limit = $request->get('quantity');
            if(!in_array($role,['Admin','Super admin'])) return redirect()->route('pin');
            if( $limit == null || $limit < 1 ) return redirect()->route('pin');

            $generated = array();

            for($i = 0; $i < $limit; $i++) {
                $pin = new Pin();
                $pin->pin = $this->pin();
                $pin->user_id = Auth::user()->id;
                $pin->save();
                array_push($generated,[
                    'pin' => $pin->pin,
                    'id' => $pin->id]);
            }

            $attr = array(
                'title' => "Generate $limit pins",
                'action' => 'generate',
                'type'  => 'pin',
                'items' => $generated);

            $user = User::where('id',Auth::user()->id)->first();

            event(new EventConversion(Auth::user(),$attr));

            return redirect()->route('pin');

        }
        return view('pin.generate');
    }

    protected function pin()
    {
        return rand(000000,999999);
    }

    protected function bool_email($email)
    {
        $bool = User::where('email',$email)->count();
        if($bool > 0)
            return true;
        return false;
    }

    protected function create_transfer($attr = array())
    {
        $transfer = new Transfer();
        $transfer->from_id = $attr['from'];
        $transfer->user_id = $attr['user'];
        $transfer->code_id = $attr['code'];
        $transfer->description = $attr['message'];
        $transfer->type = $attr['type'];
        $transfer->ip = \Request::ip();
        $transfer->save();
    }

    public function api_pins()
    {
        // $data = DB::table('pins')
        //             ->select('user_id','pins.created_at',DB::raw('count(*) as count'))
        //             ->groupBy('user_id')->get();

        $data = Pin::select('pins.user_id',DB::raw('count(*) as count'))
                    ->with('user')
                    ->groupBy('user_id')
                    // ->orderBy('created_at','DESC')
                    ->get();

        return response()->json(['data' => $data],200);
    }

    public function api_transfer_pins()
    {
        $data = Transfer::select('user_id',DB::raw('count(*) as count'))
                    // ->orderBy('updated_at','DESC')
                    ->with('user')
                    ->groupBy('user_id')
                    ->get();

        return response()->json(['data' => $data],200);
    }

}
