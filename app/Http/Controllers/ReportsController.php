<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pin;
use App\Code;
use App\User;
use App\Wallet;
use App\Payout;

class ReportsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('reports.index');
    }

    public function accounts(Request $request)
    {

        $active_users = User::whereHas('roles', function($q) {
                            $q->where('name','Premium');
                        })
                        ->count() - 1;
        $all_users = User::count();
        $count_inactive = $all_users - $active_users;

        return view('reports.accounts', [
            'count_active' => $active_users,
            'count_inactive' => $count_inactive,
            'count_accounts' => $all_users
        ]);
    }

    public function payouts()
    {
        $pending_payout = $this->wallet_pending_payout();
        $pending_count = $this->wallet_pending_count();
        $wallet_payout = $this->wallet_payout();

        return view('reports.payouts',[
            'pending_payout' => $pending_payout,
            'pending_count' => $pending_count,
            'wallet_payout' => $wallet_payout,
        ]);
    }

    public function pins() {
        $count_active_pins = Pin::where('status','active')->count();
        $count_used_pins = Pin::where('status','used')->count();
        $count_all_pins = Pin::count();
        return view('reports.pins',[
            'count_active_pins' => $count_active_pins,
            'count_used_pins' => $count_used_pins,
            'count_all_pins' => $count_all_pins
        ]);
    }
    public function codes() {
        $count_active_codes = Code::where('status','active')->count();
        $count_used_codes = Code::where('status','used')->count();
        $count_all_codes = Code::count();
        return view('reports.codes',[
            'count_active_codes' => $count_active_codes,
            'count_used_codes' => $count_used_codes,
            'count_all_codes' => $count_all_codes
        ]);
    }

    protected function count_pins($status)
    {
        return Pin::where('status',$status)->count();
    }

    protected function count_users()
    {
        return User::count();
    }

    protected function wallet_pending_payout()
    {
        return Payout::where('status','pending')
                        ->sum('amount');
    }

    protected function wallet_payout()
    {
        return Payout::where('status','success')
                        ->sum('amount');
    }

    protected function wallet_pending_count()
    {
        return Payout::where('status','pending')
                        ->count();
    }

}
