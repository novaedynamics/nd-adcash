<?php

namespace App\Http\Controllers;

use \RecursiveIteratorIterator;
use \RecursiveArrayIterator;

use App\Events\Tree\NewNode;
use Illuminate\Http\Request;
use App\Tree;
use App\User;
use Auth;
use App\Code;
use Log;
use Config;

class TreesController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

    public function index()
    {
        $tree = Auth::user()->trees()->first();
        $code = Auth::user()->codes()->get();
    	return view('trees.tree',['codes' => $code]);
    }

    public function tree()
    {	

        // $tree = Tree::where('user_id',Auth::user()->id)
        //         ->with('children.children.children.children.children.children.children.children.children.children.children.children')
        //         ->with('user')
        //         ->first();
        $tree = Tree::with('children')->where('user_id',Auth::user()->id)->first();
    	return response()->json([
		    'root' => $tree
		]);
    }

    public function post(Request $request)
    {

        $attr = array(
            'position' => $request->get('node-leg'),
            'node-name' => $request->get('node-name')
        );

        event(new NewNode(User::find(Auth::user()->id)->first(),$attr));    	
    	sleep(1);

    	return response()->json(['success' => true]);
    }

    public function tree_check(Request $request)
    {
        // return Tree::find(133)->count_children();
    }

    protected function extract($trees)
    {
        foreach ($trees as $key => $value) {
            if(is_array($value)) return $this->extract($value);
            echo $value['id'];
        }
    }

    // protected function find_key_value($array,$key,$val) 
    // {
    //     foreach($array as $item)
    //     {   
    //         if(is_array($item) && $this->find_key_value($item,$key,$val)) return true;
    //         if(isset($item[$key]) && $item[$key] == $val) return true;
    //     }
    //     return false;
    // }

    // public function tree_check(Request $request)
    // {
    //     $treeid = $request->get('treeid');
    //     $tree = Tree::where('id',$treeid)
    //             ->with('children.children.children.children.children.children.children.children.children.children.children.children')
    //             // ->with('user')
    //             ->first()->toArray();
    //             // return array($tree);
    //     echo $this->find_key_value(array($tree),'id',6);
    //             // return array_search(1,array_column(array($tree),'id'));
    // }

    // protected function find_key_value($array,$key,$val) 
    // {
    //     foreach($array as $item)
    //     {   
    //         if(is_array($item) && $this->find_key_value($item,$key,$val)) return true;
    //         if(isset($item[$key]) && $item[$key] == $val) return true;
    //     }
    //     return false;
    // }

}
