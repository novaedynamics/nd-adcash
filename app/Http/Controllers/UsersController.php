<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Tree;
use App\Code;
use App\Wallet;
use App\Pin;
use Carbon\Carbon;
use Auth;
use Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Cookie\CookieJar;
use App\Events\UpgradeAccount;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

    }

    public function update(Request $request)
    {
        if($request->ajax()) {
            if($request->isMethod('POST')) {

                $user = User::find(Auth::user()->id);

                $user->name = $request->get('name');
                $user->lastname = $request->get('lastname');
                $user->middlename = $request->get('middlename');
                $user->mobile = $request->get('mobile');
                $user->phone = $request->get('phone');
                $user->about = $request->get('about');
                $user->facebook_url = $request->get('facebook_url');
                $user->twitter_url = $request->get('twitter_url');

                $user->address = $request->get('address');
                $user->city = $request->get('city');
                $user->postal = $request->get('postal');
                $user->province = $request->get('province');
                $user->country = $request->get('country');

                $user->save();

            }
            return response()->json(['message' => 'success']);
        }
        return response()->json(['message' => 'failed','status' => 201]);
    }

    public function profile()
    {

        $user_id                = Auth::user()->id;
        $bonus_following        = Wallet::bonus_following($user_id);
        $bonus_login            = Wallet::bonus_login($user_id);
        $bonus_leveling         = Wallet::bonus_leveling($user_id);
        $bonus_pairing          = Wallet::bonus_pairing($user_id);
        $transactions           = Wallet::where('user_id',$user_id)
                                    ->where('action','credit')
                                    ->take(10)
                                    ->orderBy('created_at', 'desc')
                                    ->get();

        $flushed_out            = Wallet::where('user_id',$user_id)
                                        ->where('type','pair')
                                        ->where('action','credit')
                                        ->whereDate('created_at',Carbon::today())
                                        ->count();

        $bonus_video            = Auth::user()->wallets()->where('type','video')->count();

        $referrer = User::first();

        if(!empty(Auth::user()->parent_id)) $referrer = User::where('id', Auth::user()->parent_id)->first();

        $attr = array(
            'bonus_following'   =>  $bonus_following,
            'bonus_login'       =>  $bonus_login,
            'bonus_leveling'    =>  $bonus_leveling,
            'bonus_pairing'     =>  $bonus_pairing,
            'transactions'      =>  $transactions,
            'flushed_out'       =>  $flushed_out,
            'referrer'          =>  $referrer,
            'video'             =>  $bonus_video
        );

    	return view('users.profile', compact('attr'));

    }

    public function settings()
    {
        $users = User::find(Auth::user()->id)->first();

        // var_dump($users);
        return view('users.settings', array(
            'user' => $users));
    }

    public function upgrade(Request $request)
    {   
        if($request->isMethod('POST')) {

            $placement = User::where('email',$request->get('email'));

            if($request->get('email') == Auth::user()->email)
                return redirect()->route('account-upgrade')->with('danger','Enter a valid sponsor email');
            if($placement->count() < 1)
                return redirect()->route('account-upgrade')->with('danger','Enter a valid sponsor email');
            if(!$placement->first()->hasRole('Premium'))
                return redirect()->route('account-upgrade')->with('danger','Placement email '. $request->get('email') .' is not active');
            if($placement->first()->trees()->first()->children()->count() > 1)
                return redirect()->route('account-upgrade')->with('danger',"Sponsor's legs are filled, please use different sponsor email.");

            // Check Cross-placement
            $referrer = User::where('id',Auth::user()->parent_id); // Who referred to signup AKA user Parent ID

            $palcementid = $placement->with('trees')->first()->trees->first()->id; 
            $referrer_tree = $this->sponsor_tree($referrer->with('trees')->first()->trees->first()->id);

            if(!$this->find_key_value($referrer_tree,'id',$palcementid))
                return redirect()->route('account-upgrade')->with('danger',"Oops! Cross placement is not allowed");

            // End Cross-Placement check

            if(!$this->bool_pin($request->get('pin')) && !$this->bool_code($request->get('activation-code'))) 
                return redirect()->route('account-upgrade')->with('danger','Enter a valid pin and activation code');

            $user = Auth::user();
            $tree = $placement->first()->trees()->first();
        
            $code = Auth::user()->codes()->where('code',$request->get('activation-code'))->where('status','active')->first();
            $pin = Auth::user()->pins()->where('pin',$request->get('pin'))->where('status','active')->first();

            event(new UpgradeAccount($user,$tree,$code,$pin));
            
            return redirect()->route('dash');
        }

        $codes = Auth::user()->codes()->where('status','active')->get();
        $pins = Auth::user()->pins()->where('status','active')->get();

        return view('users.upgrade',[
                'codes' => $codes,
                'pins' => $pins
            ]);
    }

    public function uploadAvatar(Request $request)
    {
        if (!$request->hasFile('avatar-image')) {
            return $request->json(['status' => 201, 'message' => 'Invalid file'], 201);
        }

        if (!$request->file('avatar-image')->isValid()) {
            return $request->json(['status' => 201, 'message' => 'Invalid file'], 201);
        }

        $file = $request->file('avatar-image');
        $fileName = $file->getClientOriginalName();
        $fileExtension = $file->getClientOriginalExtension();
        $newFileName = time().'_'.$fileName;
        $filePath = public_path('uploads/avatars/'.$newFileName);
        $fileUrl = 'avatars/'.$newFileName;

        $img = Image::make($request->file('avatar-image'));
        $img->resize(320, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($filePath);

        $userId = Auth::user()->id;
        $user = User::find($userId);
        $user->avatar = $fileUrl;
        $user->update();

        return response()->json(['message' => 'success' ,'file' => $fileUrl],200);
    }

    public function changePassword(Request $request)
    {

            $input = $request->all();

            $user = User::find(Auth::user()->id);


            if(!Hash::check($input['current-password'], $user->password)){

                // dd('Return error with current passowrd is not match.');

            }else{

                // dd('Write here your update password code');
                $user = Auth::user();
                $user->password = bcrypt($request->get('new-password'));
                $user->save();

                return response()->json(['status' => 'success']);

            }

    }

    public function typehead(Request $request)
    {

        $data = User::select('id','name','lastname','email')->where('email', $request->get('query'))
                     ->orWhere('email', 'like', '%' .  $request->get('query') . '%')->get();
        return response()->json($data);
    }

    protected function get_tree($tree_name)
    {
        return Tree::where('name',$tree_name);
    }

    protected function bool_parent_tree($tree_name)
    {
        $tree = Tree::where('name',$tree_name);
        if($tree->count() > 0)
            return true;
        return false;
    }

    public function avatars()
    {
        $users = User::with(['wallets' => function($q) {
            $q->sum('amount');
        }])->get();

        $result = array();

        foreach($users as $u):
            $earnings = config('adcash.currency.us').number_format($u->wallets()->where('action','credit')->sum('amount'),2);
            $result[] = array(
                'id' => $u->id,
                'name' => $u->name,
                'email' => $u->email,
                'earnings' => $earnings,
                'avatar' => $u->avatar);
        endforeach;

        return $result;

    }

    protected function bool_pin($pin = null)
    {
        $count = Auth::user()->pins()->where('pin',$pin)->where('status','active')->count();
        if($count > 0)
            return true;
        return false;
    }

    protected function bool_code($code = null)
    {
        $count = Auth::user()->codes()->where('code',$code)->where('status','active')->count();
        if($count > 0)
            return true;
        return false;
    }

    protected function find_key_value($array,$key,$val) 
    {
        foreach($array as $item)
        {   
            if(is_array($item) && $this->find_key_value($item,$key,$val)) return true;
            if(isset($item[$key]) && $item[$key] == $val) return true;
        }
        return false;
    }

    public function sponsor_tree($treeid)
    {
        // $treeid = $request->get('treeid');
        // $tree = Tree::where('id',$treeid)
        //         ->with('children.children.children.children.children.children.children.children.children.children.children.children')
        //         ->first()->toArray();
        $tree = Tree::with('children')->where('id',$treeid)->first()->toArray();
        return array($tree);
    }

    public function api_users()
    {
        $users = User::with('roles')->get()->toArray();
        return response()->json(['data' => $users], 200);
    }

}