<?php

namespace App\Http\Controllers;

use App\Events\EventConversion;
use Illuminate\Http\Request;
use Auth;
use App\Conversion;
use App\Wallet;
use Config;
use Carbon\Carbon;
class VideosController extends Controller
{

	public $limit;
	public $amount;

    public function __construct()
	{
		$this->middleware('auth');
		$this->limit = 2; // 2 videos limit per day only.
		$this->amount = 1;
	}

	public function index(Request $request)
	{
		$video = $request->get('video');
		if(!$this->doValidate($video)) return response()->json(['status' => 'invalid']);
		if($this->bool_watched($video)) return response()->json(['status' => 'watched']);
		if($this->bool_limit_daily(2)) return response()->json(['status' => 'limit']);
		if($this->bool_limit(10)) return response()->json(['status' => 'limit']);

		if($request->get('verify') != null && $request->get('verify')) {
			$this->credit_wallet($video);
			$this->create_video($request->get('video'));
			return response()->json(['status' => 'success']);
		}

		return response()->json(['status' => true]);
	}

	protected function credit_wallet($video)
	{
		$wallet = new Wallet();
        $wallet->amount = $this->amount;
        $wallet->action = 'credit';
        $wallet->type = 'video';
        $wallet->status = 'Y';
        $wallet->description = config('adcash.description.bonus.video');
        $wallet->user_id = Auth::user()->id;
        $wallet->ip = \Request::ip();
        $wallet->save();
	}

	protected function create_video($video)
	{
		$generated = array();
		array_push($generated,[
                    'video' => $video,
                    'id' => $video]);
		$attr = array(
                'title' => "Generate watched video",
                'action' => 'generate',
                'type'  => 'video',
                'items' => $generated);

        event(new EventConversion(Auth::user(),$attr));
		return true;
	}

	protected function doValidate($video)
	{
		if(in_array($video,['SaqmmW7SIOM','K9hm4zwfheM','kSBG7Z29RI0','YEWLjHoJajw','gIQK5iHO2Ls']))
			return true;
		return false;
	}

	protected function bool_watched($video)
	{
		$count = Conversion::where('user_id',Auth::user()->id)
					->where('description',$video)
					->whereDate('created_at',Carbon::today())
					->where('type','video')
					->count();
		if($count > 0)
			return true;
		return false;
	}

	protected function bool_limit_daily($limit)
	{
		$count = Conversion::where('user_id',Auth::user()->id)
					->whereDate('created_at',Carbon::today())
					->where('type','video')
					->count();
		if($count > $limit)
			return true;
		return false;
	}

	protected function bool_limit($limit)
	{
		// $count = Conversion::where('user_id',Auth::user()->id)
		// 			->where('type','video')
		// 			->count();

		$count = Wallet::where('user_id', Auth::user()->id)
					->where('type','video')
					->count();

		if($count >= $limit)
			return true;
		return false;
	}


}
