<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use App\Wallet;
use Auth;
use Config;
use DB;

class WalletsController extends Controller
{
    
	public function __construct()
    {
        $this->middleware('auth');
    }

	public function index(Request $request) 
	{
		$limit = 10;
		$sort = 'DESC';
		$range = "today";

		if($request->get('limit')) 	$limit = $request->get('limit');
		if($request->get('sort')) 	$sort = $request->get('sort');

		if($range == 'today') 	$top = $this->today($sort,$limit);
		if($range == 'week') 	$top = $this->week($sort,$limit);
		if($range == 'month') 	$top = $this->month($sort,$limit);

        return response()->json($top);
	}

	protected function today($sort,$limit)
	{
		return Wallet::select('user_id',DB::raw('SUM(amount) as earnings'))
				->whereDate('created_at', Carbon::today())
                ->with('user')
                ->groupBy('user_id')
                ->orderBy('earnings', $sort)
                ->take($limit);
	}

	protected function week($sort,$limit)
	{
		return Wallet::select('user_id',DB::raw('SUM(amount) as earnings'))
				->whereBetween('created_at', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])
                ->with('user')
                ->groupBy('user_id')
                ->orderBy('earnings', $sort)
                ->take($limit);
	}

	protected function month($sort,$limit)
	{
		return Wallet::select('user_id',DB::raw('SUM(amount) as earnings'))
				->whereBetween('created_at', [Carbon::now()->startOfMonth(),Carbon::now()->endOfMonth()])
                ->with('user')
                ->groupBy('user_id')
                ->orderBy('earnings', $sort)
                ->take($limit)
                ->get();
	}

}
