<?php

namespace App\Listeners\Bonus;

use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Tree;
use App\User;
use App\Wallet;
use Log;
use Auth;
use Config;
use Carbon\Carbon;

class CreditDailyGroupLogin
{

    public $user;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $this->user = Auth::user();

        if($this->bool_user_has_roles(['Premium']))
        {
            $this->init();
        }

        
    }

    protected function init()
    {
        $trees = $this->trees()->get();
        
        foreach($trees as $tree)
        {
            
            // Current tree add credit to wallet
            if($this->bool_allow_credit($tree->id)) {
                $amount = config('adcash.credits.login.amount');
                $description = config('adcash.description.bonus.login');
                $this->credit_user(array(
                        'user_id' => $tree->user_id,
                        'tree_id' => $tree->id,
                        'amount' => $amount,
                        'description' => $description));


                // parents

                if($this->bool_tree($tree->parent_id)) {

                    // This is level 1 parent
                    if(!$this->bool_tree_max_credit($tree->parent_id)) {

                        $parent = $this->tree($tree->parent_id)->first();

                        $amount = config('adcash.credits.login-first.amount');
                        $description = config('adcash.description.bonus.login-first');

                        $this->credit_user(array(
                            'user_id' => $parent->user_id,
                            'tree_id' => $parent->id,
                            'amount' => $amount,
                            'description' => $description));

                        // Second level
                        if($this->bool_tree($parent->parent_id)) {

                            if($this->bool_tree_max_credit($tree->id)) {
                                $parent_second = $this->tree($parent->parent_id)->first();

                                $amount = config('adcash.credits.login-second.amount');
                                $description = config('adcash.description.bonus.login-second');

                                $this->credit_user(array(
                                    'user_id' => $parent_second->user_id,
                                    'tree_id' => $parent_second->id,
                                    'amount' => $amount,
                                    'description' => $description));
                            }
                            
                        }

                    }

                }

            }

        }

    }

    protected function trees()
    {
        return Tree::where('user_id',$this->user->id);
    }

    protected function tree($tree_id)
    {
        return Tree::where('id',$tree_id);
    }

    protected function bool_tree($tree_id)
    {
        $bool = Tree::where('id',$tree_id)->count();
        if($bool > 0)
            return true;
        return false;
    }

    protected function credit_user($attr = array())
    {
        $wallet = new Wallet();
        $wallet->user_id = $attr['user_id'];
        $wallet->tree_id = $attr['tree_id'];
        $wallet->amount = $attr['amount'];
        $wallet->description = $attr['description'];
        $wallet->type = 'login';
        $wallet->action = 'credit';
        return $wallet->save();        
    }

    protected function bool_user_has_roles($roles) {
        $role = Auth::user()->roles->first()->name;
        if(in_array($role,$roles))
            return true;
        return false;
    }

    protected function bool_allow_credit($tree_id)
    {
        if($this->bool_tree_credited($tree_id) || $this->bool_tree_max_credit($tree_id))
            return false;
        return true;
    }

    protected function bool_tree_credited($tree_id)
    {
        // Count the number of credits of tree today
        $credits = Wallet::where('created_at','>=',Carbon::today())
                    ->where('tree_id',$tree_id)
                    ->count();
        if($credits > 0)
            return true;
        return false;
    }

    protected function bool_tree_max_credit($tree_id)
    {
        // Check wallet if maxed out
        $sum = Wallet::where('tree_id',$tree_id)
                    ->where('type','login')
                    ->where('action','credit')
                    ->sum('amount');

        $credit = config('adcash.credits.login.amount');
        $max = config('adcash.max.login.amount');

        if(($sum + $credit) <= $max)
            return false;
            
        return true;
    }

}
