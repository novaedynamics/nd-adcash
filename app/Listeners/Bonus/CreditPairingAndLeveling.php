<?php

namespace App\Listeners\Bonus;

use App\Events\UpgradeAccount;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use App\Code;
use App\Tree;
use App\Wallet;
use Carbon\Carbon;
use Config;
use Auth;
use Log;


class CreditPairingAndLeveling
{
    public $tree;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpgradeAccount  $event
     * @return void
     */
    public function handle(UpgradeAccount $event)
    {
        $this->tree = Auth::user()->trees()->first();
        $this->init();
    }

    protected function data_tree($tree_id)
    {
        // $tree = Tree::where('id',$tree_id)
        //         ->with('children.children.children.children.children.children.children.children.children.children.children.children')
        //         ->first();
        $tree = Tree::with('children')->where('id',$tree_id)->first();
        return $tree;
    }

    protected function treeOut($tree, $level = 0, $counts = [])
    {
        if(!isset($counts[$level])) $counts[$level] = 0;
        foreach($tree as $branch => $twig)
        {
            $counts[$level]++;
            if(is_array($twig)) {
                $counts = $this->treeOut($twig, $level+1, $counts);
            }
        }
        return $counts;
    }

    protected function remove_odd_index($data)
    {
        $arr = array();
        foreach($data as $key => $value) 
        { 
            if($key&1) unset($data[$key]);
        }

        foreach ($data as $key => $value) {
            array_push($arr,$value);
        }

        return $arr;
    }

    public function count_level()
    {
        $this->init();
    }

    protected function get_parent_level($tree_id,$limit = 0)
    {
        $parents = array();
        $current_id = $tree_id;
        for($i = 0; $i < $limit; $i++)
        {
            $tree = Tree::where('id',$current_id)->first();
            if($tree->parent_id == null) break;
            $parents[]  = $tree->parent_id;
            $current_id = $tree->parent_id;
        }
        return $parents;
    }

    protected function get_level_and_element_counts($tree_id)
    {
        $data = array(
            $this->data_tree($tree_id)->toArray()
        );

        $parse = $this->treeOut($data);
        return $this->remove_odd_index($parse);
    }

    protected function init()
    {
        // $parents = $this->get_parent_level(Auth::user()->trees()->first()->id,config('adcash.limit.leveling'));
        $parents = $this->get_parent_level(Auth::user()->trees()->first()->id,1000);
        
        \Log::info('parents',[$parents]);

        $level = 1;
        foreach ($parents as $key => $tree_id) {
            if($level == 1) $this->level_bonus_one($tree_id);
            if($level != 1) $this->level_bonus($tree_id,$level,$parents);
            $level++;
        }
    }

    protected function level_bonus_one($tree_id)
    {
        $tree = Tree::where('id',$tree_id)->first();
        if($tree->children->count() >1)
        {
            $user = User::where('id',$tree->user_id)->first();
            $this->credit_pairing_bonus(array(
                'description' => config('adcash.description.bonus.pairing'),
                'amount' => config('adcash.credits.pairing.amount'),
                'user_id' => $user->id,
                'tree_id' => $tree_id,
                'from_tree_id' => $this->tree->id));
            $this->credit_leveling_bonus(array(
                'description' => config('adcash.description.bonus.leveling'),
                'amount' => config('adcash.credits.leveling.amount'),
                'user_id' => $user->id,
                'tree_id' => $tree_id,
                'from_tree_id' => $this->tree->id));
        }
    }

    protected function level_bonus($tree_id,$level,$parents = array())
    {

        // Legs
        $leg_L = null;
        $leg_R = null;
        $tree_leg_L = null;
        $tree_leg_R = null;
        $leg_L_data = null;
        $leg_R_data = null;
        $leg_L_count = 0;
        $leg_R_count = 0;

        // Get node
        $tree = Tree::where('id',$tree_id)->first();

        // Get legs
        if($tree->children()->where('position','L')->count())   $leg_L = $tree->children()->where('position','L')->get();
        if($tree->children()->where('position','R')->count())   $leg_R = $tree->children()->where('position','R')->get();

        // Set legs
        ($leg_L) ? $leg_L = $leg_L[0]->id : null;
        ($leg_R) ? $leg_R = $leg_R[0]->id : null;

        // 2/10/2018 --- count each level children of children
        ($leg_L) ? $leg_L_count = Tree::find($leg_L)->count_children() : 0;
        ($leg_R) ? $leg_R_count = Tree::find($leg_R)->count_children() : 0;

        // Get data for each leg
        ($leg_L) ? $leg_L_data = Tree::where('id',$leg_L)->first() : null;
        ($leg_R) ? $leg_R_data = Tree::where('id',$leg_R)->first() : null;

        // Get data tree; index = level; value = element count
        ( $leg_L_data ) ? $tree_leg_L = $this->get_level_and_element_counts($leg_L_data->id) : null;
        ( $leg_R_data ) ? $tree_leg_R = $this->get_level_and_element_counts($leg_R_data->id) : null;

        // Remove extra level
        if(!empty($tree_leg_L)) array_pop($tree_leg_L);
        if(!empty($tree_leg_R)) array_pop($tree_leg_R);

        // Get current level element count of each legs set to 0 if empty
        if(empty($tree_leg_R)) $tree_leg_R = 0;
        if(empty($tree_leg_L)) $tree_leg_L = 0;

        

        // Check for values
        if(!empty($tree_leg_R))
            (count($tree_leg_R) >= $level) ? $tree_leg_R = $tree_leg_R[$level-1] : $tree_leg_R = 0;
        else
            $tree_leg_R = 0;

        if(!empty($tree_leg_L))
            (count($tree_leg_L) >= $level) ? $tree_leg_L = $tree_leg_L[$level-1] : $tree_leg_L = 0;
        else
            $tree_leg_L = 0;        



        // Get your node position from parent leg
        $position = $this->get_node_position($parents[$level-2]);

        $user = User::where('id',$tree->user_id);

        $pairing = array(
            'description' => "Congratulations! You just received a pairing bonus from level $level.",
            // 'description' => config('adcash.description.bonus.pairing'),
            'amount' => config('adcash.credits.pairing.amount'),
            'user_id' => $user->first()->id,
            'tree_id' => $tree_id,
            'from_tree_id' => $this->tree->id);

        $leveling = array(
            'description' => 'Congratulations! You just received your level '.$level.' bonus.',
            'amount' => config('adcash.credits.leveling.amount'),
            'user_id' => $user->first()->id,
            'tree_id' => $tree_id,
            'from_tree_id' => $this->tree->id);

        // Conditional if add credit to parent as pairing bonus

        if (!$this->bool_max_daily_pairing($user->first()->id)) {


            // \Log::info('position',[$position]);
            // \Log::info('level',[$level]);
            // \Log::info('tree_leg_R',[$tree_leg_R]);
            // \Log::info('tree_leg_L',[$tree_leg_L]);

            // $this->credit_pairing_bonus($pairing);

            // ($leg_L) ? $leg_L_count = Tree::find($leg_L[0]->id)->count_children() : 0;
            // ($leg_R) ? $leg_R_count = Tree::find($leg_R[0]->id)->count_children() : 0;

            if($position == "L") {
                if( $leg_L_count <= $leg_R_count )    $this->credit_pairing_bonus($pairing);
            } else {
                if( $leg_R_count <= $leg_L_count )    $this->credit_pairing_bonus($pairing);
            }

        }

        if (!$this->bool_max_leveling($level)) {

            if( $tree_leg_R > 0 && $tree_leg_L > 0 ) {
                $count = Wallet::where('user_id',$user->first()->id)
                                ->where('type','level')
                                ->count();

                if($count >= $level) return;
                $this->credit_leveling_bonus($leveling);
            }

        }


    }

    protected function get_node_position($tree_id)
    {
        // Retrive your node leg position from parent
        $tree = Tree::where('id',$tree_id)->first();
        return $tree->position;
    }

    protected function credit_pairing_bonus($attr = array())
    {
        // \Log::info('crediting pair',$attr);
        $wallet = new Wallet();
        $wallet->description = $attr['description'];
        $wallet->amount = $attr['amount'];
        $wallet->action = 'credit';
        $wallet->type = 'pair';
        $wallet->user_id = $attr['user_id'];
        $wallet->tree_id = $attr['tree_id'];
        $wallet->from_tree_id = $attr['from_tree_id'];
        $wallet->save();
    }


    protected function credit_leveling_bonus($attr = array())
    {
        $wallet = new Wallet();
        $wallet->description = $attr['description'];
        $wallet->amount = $attr['amount'];
        $wallet->action = 'credit';
        $wallet->type = 'level';
        $wallet->user_id = $attr['user_id'];
        $wallet->tree_id = $attr['tree_id'];
        $wallet->from_tree_id = $attr['from_tree_id'];
        $wallet->save();
    }


    protected function bool_max_daily_pairing($user_id)
    {
        $max_daily = config('adcash.limit.pairing.day');
        $count = Wallet::where('user_id',$user_id)
                                        ->where('type','pair')
                                        ->where('action','credit')
                                        ->whereDate('created_at',Carbon::today()->timezone(Config::get('app.timezone')))
                                        ->count();
        if($count >= $max_daily) return true;
        return false;
    }

    protected function bool_max_leveling($level)
    {
        // max level bonus
        $max_level = config('adcash.limit.leveling');
        if($level > $max_level) return true;
        return false;
    }

}
