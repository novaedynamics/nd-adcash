<?php

namespace App\Listeners\Bonus;

use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Carbon\Carbon;
use App\User;
use App\Tree;
use App\Wallet;
use Auth;
use Log;
use Config;

class CreditSecondLevel
{

    public $user;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $this->user = Auth::user();
        $this->init();
    }

    protected function bool_has_roles($roles = array())
    {
        if(in_array($this->user->roles->first()->name,$roles))
            return true;
        return false;
    }

    protected function init()
    {
        if($this->bool_has_roles(['Premium'])) {            // If user have premium role
            $tree = $this->user->trees->first();
            if($this->bool_tree($tree->parent_id)) {        // If parent tree exist
                $tree_parent = $this->tree($tree->parent_id)->first();
                // if(!$this->bool_tree_max_credit($tree->parent_id)) { // Check if tree is not max limit
                //     if(!$this->bool_daily($tree->id,$tree_parent->id)) {

                //         $amount = config('adcash.credits.login-first.amount');
                //         $description = config('adcash.description.bonus.login-first');

                //         $this->add_tree_credit(array(
                //             'user_id' => $tree_parent->user_id,
                //             'tree_id' => $tree_parent->id,
                //             'from_tree_id' => $tree->id,
                //             'description' =>$description,
                //             'amount' => $amount));
                        
                //     }
                // }

                // Parent exist, check parent parent
                if($this->bool_tree($tree_parent->parent_id)) {
                    $parent = $this->tree($tree_parent->parent_id)->first();
                    if(!$this->bool_tree_max_credit($tree_parent->parent_id)) { // Check if tree is not max limit
                        if(!$this->bool_daily($tree->id,$tree_parent->parent_id)) {

                            $amount = config('adcash.credits.login-second.amount');
                            $description = config('adcash.description.bonus.login-second');

                            $this->add_tree_credit(array(
                                'user_id' => $parent->user_id,
                                'tree_id' => $parent->id,
                                'from_tree_id' => $tree->id,
                                'description' =>$description,
                                'amount' => $amount));

                        }
                    }
                }

            }
        }
    }

    protected function tree($tree_id)
    {
        return Tree::where('id',$tree_id);
    }

    protected function bool_tree($tree_id)
    {
        $count = Tree::where('id',$tree_id)->count();
        if($count > 0)
            return true;
        return false;
    }

    protected function bool_tree_max_credit($tree_id)
    {

        $sum = Wallet::where('tree_id',$tree_id)
                    ->where('type','login')
                    ->where('action','credit')
                    ->sum('amount');

        $credit = config('adcash.credits.login.amount');
        $max = config('adcash.max.login.amount');

        if(($sum + $credit) <= $max)
            return false;
            
        return true;

    }

    protected function bool_daily($from_tree_id,$to_tree_id)
    {
        $count = Wallet::where('tree_id',$to_tree_id)
                    ->where('from_tree_id',$from_tree_id)
                    ->where('created_at','>=',Carbon::today())->count();
        // \Log::info($count);
        if($count > 0)
            return true;
        return false;
    }

    protected function add_tree_credit($attr = array())
    {
        $wallet = new Wallet();
        $wallet->description = $attr['description'];
        $wallet->amount = $attr['amount'];
        $wallet->action = 'credit';
        $wallet->type = 'login';
        $wallet->user_id = $attr['user_id'];
        $wallet->tree_id = $attr['tree_id'];
        $wallet->from_tree_id = $attr['from_tree_id'];
        $wallet->save();
    }

}
