<?php

namespace App\Listeners\Bonus;

use App\Events\UpgradeAccount;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Tree;
use App\User;
use App\Wallet;
use Log;
use Carbon\Carbon;

class DailyGroupLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */

    public $user;

    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpgradeAccount  $event
     * @return void
     */
    public function handle(UpgradeAccount $event)
    {
        $this->user = $event->user;
        $this->init();
    }

    protected function trees()
    {
        // return all user trees
        return Tree::where('user_id',$this->user->id);
    }

    protected function init()
    {
        $trees = $this->trees()->get();

        foreach($trees as $tree)
        {
            $tree_id =  $tree->id;
            $tree_parent_id = $tree->parent_id;

            // Get the tree parent object
            $parent_tree_first = $this->tree($tree_parent_id);                   // 1st level parent
            $parent_tree_second = $this->tree($parent_tree_first->parent_id);    // 2nd level parent

            // first level credit bonus
            if($tree_parent_id && 
                !$this->bool_tree_login($parent_tree_first->id)) {
                
                $this->credit_bonus(array(
                    'user_id' => $parent_tree_first->user_id,
                    'tree_id' => $parent_tree_first->id,
                    'description' => config('adcash.description.bonus.login-first'),
                    'amount' => config('adcash.credits.login-first.amount'),
                ));
            }
            
            if($parent_tree_first->parent_id && 
                !$this->bool_tree_login($parent_tree_first->parent_id)) {

                $this->credit_bonus(array(
                    'user_id' => $parent_tree_second->user_id,
                    'tree_id' => $parent_tree_second->id,
                    'description' => config('adcash.description.bonus.login-second'),
                    'amount' => config('adcash.credits.login-second.amount'),
                ));
            }
            

        }
    }

    protected function tree($tree_id)
    {
        return Tree::where('id',$tree_id)->first();
    }

    protected function user($user_id)
    {
        return User::where('id',$user_id)->first();
    }

    protected function credit_bonus($attr = array())
    {
        $wallet = new Wallet();
        $wallet->user_id = $attr['user_id'];
        $wallet->tree_id = $attr['tree_id'];
        $wallet->type = 'login';
        $wallet->action = 'credit';
        $wallet->description = $attr['description'];
        $wallet->amount = $attr['amount'];
        $wallet->ip = \Request::ip();
        $wallet->save();        
    }

    protected function bool_tree($tree_id)
    {
        $tree = Tree::where('id',$tree_id);
        if($tree->count() > 0)
            return true;
        return false;
    }

    protected function bool_tree_max_login() {}

    protected function bool_tree_login($tree_id) {
        // Check if wallet already credited
        $bool = Wallet::where('created_at','>=',Carbon::today())->where('tree_id',$tree_id)->count();
        if($bool > 0)
            return true;
        return false;
    }

}
