<?php

namespace App\Listeners\Converion;

use App\Events\EventConversion;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Auth;
use Log;
use App\User;
use App\Conversion;
use App\Code;

class NewConversion
{

    public $user;
    public $attr;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EventConversion  $event
     * @return void
     */
    public function handle(EventConversion $event)
    {
        $this->user = $event->user;
        $this->attr = $event->attr;
        $this->create($event);
    }

    protected function create($event)
    {
        $this->NewConversion($event->user->id,$event->attr,$event->attr['type']);
    }

    protected function NewConversion($user_id,$attr,$type)
    {
        $head_id = $this->ConversionHeader($user_id,$attr['title'],$type,$attr['action']);
        if($head_id) $this->ConversionDetails($head_id,$user_id,$attr['items'],$type,$attr['action']);
    }

    protected function ConversionDetails($head_id,$user_id,$items = array(),$type,$action)
    {
        foreach ($items as $item) {

            $description = "";

            if(isset($item['pin'])) $description = $item['pin'];
            if(isset($item['code'])) $description = $item['code'];
            if(isset($item['video'])) $description = $item['video'];

            $conversion = new Conversion();
            $conversion->title = $item['id'];
            $conversion->description = $description;
            $conversion->type = $type;
            $conversion->action = $action;
            $conversion->parent_id = $head_id;
            $conversion->user_id = $user_id;
            $conversion->save();
        }
    }    

    protected function ConversionHeader($user_id,$title,$type,$action)
    {
        $conversion = new Conversion();
        $conversion->title = $title;
        $conversion->type = $type;
        $conversion->action = $action;
        $conversion->user_id = $user_id;
        $conversion->save();
        return $conversion->id;
    }    

}
