<?php

namespace App\Listeners;

use App\Events\UpgradeAccount;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;
use App\User;
use App\Wallet;
use Illuminate\Http\Request;
use Config;

class CreditReferralBonus
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpgradeAccount  $event
     * @return void
     */
    public function handle(UpgradeAccount $event)
    {
        $user = Auth::user();

        $wallet = new Wallet();
        $wallet->user_id = $user->parent_id;
        $wallet->tree_id = $event->tree->id;
        $wallet->type = 'invite';
        $wallet->action = 'credit';
        $wallet->description = config('adcash.description.bonus.invite');
        $wallet->amount = config('adcash.credits.invite.amount');
        $wallet->ip = \Request::ip();
        $wallet->save();        
    }
}