<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Request;
use App\Wallet;

class CreditSignupBonus
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        // Signup Bonus
        $wallet = new Wallet();
        $wallet->user_id = $event->user->id;
        $wallet->amount = config('adcash.credits.signup.amount');
        $wallet->description = config('adcash.description.bonus.signup');
        $wallet->type = 'signup';
        $wallet->action = 'credit';
        $wallet->ip = Request::ip();
        $wallet->save();

    }
}
