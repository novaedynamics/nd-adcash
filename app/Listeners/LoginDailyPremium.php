<?php

namespace App\Listeners;

use App\Events\UpgradeAccount;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;
use App\Tree;
use App\Wallet;
use Config;
use Carbon\Carbon;

class LoginDailyPremium
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpgradeAccount  $event
     * @return void
     */
    public function handle(UpgradeAccount $event)
    {

        if(!in_array(Auth::user()->roles->first()->name,['Ban','Regular'])) {
            
            $tree = Tree::where('created_at','>=',Carbon::today())->where('user_id',$event->user->id)->first();
            $login_count = Wallet::where('created_at','>=',Carbon::today())->count();
            $sum = Wallet::where('user_id',Auth::user()->id)->where('action','credit')->where('type','login')->sum('amount');

            $credit = config('adcash.credits.login.amount');
            $max = config('adcash.max.login.amount');

            // Get first node 

            if(($sum + $credit) < $max) {
                $wallet = new Wallet();
                $wallet->amount = $credit;
                $wallet->type = 'login';
                $wallet->action = 'credit';
                $wallet->description = config('adcash.description.bonus.login');
                $wallet->user_id = Auth::user()->id;
                $wallet->tree_id = $tree->id;
                $wallet->save();
            }

        }
    }
}
