<?php

namespace App\Listeners\Logs;

use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Auth;
// use Log;
use App\Log;


class LogLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $this->add_log();
    }

    protected function add_log()
    {
        $log = new Log();
        $log->user_id = Auth::user()->id;
        $log->description = "New login session by " . Auth::user()->name;
        $log->model = "login";
        $log->save();
    }

}
