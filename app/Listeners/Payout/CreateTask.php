<?php

namespace App\Listeners\Payout;

use App\Events\Payout\PayoutRequest;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use App\Task;
use Log;

class CreateTask
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PayoutRequest  $event
     * @return void
     */
    public function handle(PayoutRequest $event)
    {
        $task = new Task();
        $task->title = $event->attr['title'];
        $task->body = $event->attr['body'];
        $task->user_id = $event->user->id;
        $task->status = 'pending';
        $task->save();
    }
}
