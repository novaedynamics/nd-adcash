<?php

namespace App\Listeners\Payout;

use App\Events\Payout\PayoutRequest;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Wallet;
use Log;

class CreateWalletRecord
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PayoutRequest  $event
     * @return void
     */
    public function handle(PayoutRequest $event)
    {
        $wallet = new Wallet();
        $wallet->status = 'P';
        $wallet->type = 'payout';
        $wallet->action = 'debit';
        $wallet->amount = $event->attr['amount'];
        $wallet->description = 'Your payout request status via ' . $event->attr['method'];
        $wallet->user_id = $event->user->id;
        $wallet->tree_id = 0;
        $wallet->save();
    }
}
