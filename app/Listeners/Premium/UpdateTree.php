<?php

namespace App\Listeners\Premium;

use App\Events\UpgradeAccount;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Tree;
use App\User;
use Auth;

class UpdateTree
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpgradeAccount  $event
     * @return void
     */
    public function handle(UpgradeAccount $event)
    {
        $tree = $this->tree();
        $tree->parent_id = $event->tree->id;
        $tree->position = $this->set_position($event->tree->id);
        $tree->save();
    }

    // Get the account tree
    protected function tree()
    {
        return Auth::user()->trees()->first();
    }

    protected function bool_tree($user_id)
    {
        $bool = $this->tree($user_id);
        if($bool->count() > 0)
            return true;
        return false;
    }

    public function set_position($tree_id)
    {
        $count =Tree::find($tree_id)->children()->count();
        if($count > 0)
            return "R";
        return "L";
    }

}