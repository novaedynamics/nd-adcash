<?php

namespace App\Listeners;

use App\Events\CodeTransfer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Log;
use App\Transfer;
use App\Mail\SendConfirmTransferCodeEmail;
use Mail;

class TransferSendConfirmEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CodeTransfer  $event
     * @return void
     */
    public function handle(CodeTransfer $event)
    {
        // \Log::info('TransferSendConfirmEmail',['dispatch' => $event->user->email]);
        $transfer = Transfer::where('code_id',$event->code->id)->first();
        Mail::to($event->user->email)->send(new SendConfirmTransferCodeEmail($transfer));
    }
}
