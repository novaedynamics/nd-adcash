<?php

namespace App\Listeners\Tree;

use App\Events\Tree\NewNode;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

use Auth;
use Log;
use App\User;
use App\Tree;
use App\Wallet;

class CreateTree
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewNode  $event
     * @return void
     */
    public function handle(NewNode $event)
    {        
        $parent_node = Tree::where('name',$event->attr['node-name'])->first();
        $parent_parent = Tree::where('parent_id',$parent_node->parent_id)->first();
        $tree = new Tree();
        $tree->user_id = $event->user->id;
        $tree->position = $event->attr['position'];
        $tree->parent_id = $parent_node->id;
        $tree->name = 'node_'.time();
        $tree->save();



        $this->credit_node($parent_node->id,$parent_node->user_id,config('adcash.description.bonus.login-first'),
            config('adcash.credits.login-first.amount'));
        $this->credit_node($parent_parent->id,$parent_parent->user_id,config('adcash.description.bonus.login-second'),
            config('adcash.credits.login-second.amount'));

    }

    protected function credit_node($node_id,$user_id,$description,$amount)
    {   

        \Log::info('node',[$node_id,$user_id,$description,$amount]);

        if(!$node_id) return false;

        // $wallet = new Wallet();
        // $wallet->description = $description;
        // $wallet->ip = \Request::ip();
        // $wallet->amount = $amount;
        // $wallet->action = 'credit';
        // $wallet->type = 'login';
        // $wallet->tree_id = $node_id;
        // $wallet->user_id = $user_id;
        // $wallet->save();

        // return true;
    }

}
