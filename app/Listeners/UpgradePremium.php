<?php

namespace App\Listeners;

use App\Events\UpgradeAccount;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App\Role;
use App\Code;
use Log;

class UpgradePremium
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpgradePremium  $event
     * @return void
     */
    public function handle(UpgradeAccount $event)
    {        
        $event->user->roles()->detach();
        $event->user->roles()->attach(Role::where('name','Premium')->first());
        $event->code->status = 'used';
        $event->code->save();
        $event->pin->status = 'used';
        $event->pin->save();
    }
}