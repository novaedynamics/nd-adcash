<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pin extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function transfers()
    {
        return $this->hasMany('App\Transfer');
    }

    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            
        });

        self::created(function($model){
            
        });

        self::updating(function($model){
            
        });

        self::updated(function($model){
            
        });

        self::deleting(function($model){
            
        });

        self::deleted(function($model){
            
        });
    }

}