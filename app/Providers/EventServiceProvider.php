<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],
        'App\Events\UserRegistered' => [
            'App\Listeners\sendActivationCode',
            'App\Listeners\CreditSignupBonus',
            'App\Listeners\CreateTreeNode'
        ],
        'Illuminate\Auth\Events\Login' => [
            'App\Listeners\Bonus\CreditLoginBonus',
            'App\Listeners\Bonus\CreditFirstLevel',
            'App\Listeners\Bonus\CreditSecondLevel',
        ],
        'App\Events\CodeTransfer' => [
            'App\Listeners\TransferSendConfirmEmail',
        ],
        'App\Events\UpgradeAccount' => [
            'App\Listeners\UpgradePremium',
            'App\Listeners\Premium\UpdateTree',
            'App\Listeners\Bonus\Login\CreditLoginBonusPremium',
            'App\Listeners\Bonus\Login\CreditFirstLevelPremium',
            'App\Listeners\Bonus\Login\CreditSecondLevelPremium',
            'App\Listeners\Bonus\CreditPairingAndLeveling',
            'App\Listeners\CreditReferralBonus',

        ],
        'App\Events\Payout\PayoutRequest' => [
            'App\Listeners\Payout\CreateWalletRecord',
        ],
        'App\Events\Tree\NewNode' => [
            'App\Listeners\Tree\CreateTree'
        ],
        'App\Events\EventConversion' => [
            'App\Listeners\Converion\NewConversion'
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
