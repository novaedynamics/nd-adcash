<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;

class Transfer extends Model
{

    use Notifiable;

    protected $fillable = [
        'from_to','to_id','status','code_id','token'
    ];

    protected static function boot()
    {
        parent::boot();

        self::creating(function($model) {

        });

        self::updating(function($model) {

        });

    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function pin()
    {
        return $this->belongsTo('App\Pin','pin_id');
    }

    public function code()
    {
        return $this->belongsTo('App\Code','code_id');
    }

}
