<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Http\Request;
// use Kalnoy\Nestedset\NodeTrait;

class Tree extends Model
{

    use Notifiable;
    // use NodeTrait;

    protected $guarded = ['id'];

    protected $fillable = [
        // List of fillable fields
    	// 'name','left','right','parent_id','user_id','wallet_id'
    ];

    protected $hidden = [
        // 'id','created_at','updated_at','parent_id','user_id','position'
    ];

    protected static function boot()
    {
        parent::boot();

        self::creating(function($model) {

        });

        self::updating(function($model) {

        });

    }

    public function parent()
    {
        return $this->belongsTo('App\Tree')->with('parent');
    }

    public function children()
    {        
        return $this->hasMany('App\Tree', 'parent_id','id')->with('children');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function wallets()
    {
        return $this->hasMany('App\Tree');
    }

    // public function count_children()
    // {
        
    //     $count = 0;
    //     $query = $this->children();

    //     foreach($query->get() as $child)
    //     {
    //         $count += $child->count_children() + 1;
    //     }

    //     return $count;
    // }

    public function count_children()
    {
        
        $count = 0;
        $query = $this->childrenOne();

        foreach($query->get() as $child)
        {
            $count += $child->count_children() + 1;
        }

        return $count;
    }

    public function childrenOne()
    {        
        return $this->hasMany('App\Tree', 'parent_id','id');
    }

}
