<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','parent_id','avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $bytes = openssl_random_pseudo_bytes(32);
            $hash = md5(base64_encode($bytes));
            $model->referral_id = $hash;
        });

        self::created(function($model){
            // ... code here
        });

        self::updating(function($model){
            // ... code here
        });

        self::updated(function($model){
            // ... code here
        });

        self::deleting(function($model){
            // ... code here
        });

        self::deleted(function($model){
            // ... code here
        });
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role','user_role','user_id','role_id');
    }

    public function hasAnyRole($roles)
    {
        if(in_array($roles)) {
            foreach ($roles as $role) {
                if($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }

    public function hasRole($role)
    {
        if($this->roles()->where('name', $role)->first()) {
            return true;
        }
        return false;
    }

    public function trees() 
    {
        return $this->hasMany('App\Tree');
    }

    public function codes() 
    {
        return $this->hasMany('App\Code');
    }

    public function logs()
    {
        return $this->hasMany('App\User');
    }

    public function wallets()
    {
        return $this->hasMany('App\Wallet');
    }

    public function directs()
    {
        return $this->hasMany('App\User', 'parent_id','id');
    }

    public function pins()
    {
        return $this->hasMany('App\Pin');
    }

    public function transfers()
    {
        return $this->hasMany('App\Transfer');
    }

    public function setFullNameAttribute($value)
    {
        $this->attributes['full_name'] = $value;
    }

    public function getFullNameAttribute()
    {
        return "{$this->name} {$this->lastname}";
    }

    public function payouts()
    {
        return $this->hasMany('App\Payout');
    }

    public function conversions()
    {
        return $this->hasMany('App\Conversion');
    }

}
