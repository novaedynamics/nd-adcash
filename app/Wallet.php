<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;

class Wallet extends Model
{

    use Notifiable;

    protected $fillable = [
        'amount','description','action','ip','type','user_id','tree_id'
    ];

    protected static function boot()
    {
        parent::boot();

        self::creating(function($model) {
            $model->ip = \Request::ip();
            // $user->referral_id = str_random(10);
        });

        self::updating(function($model) {
            $model->ip = \Request::ip();
        });

    }

    public function user()
    {
      return $this->belongsTo('App\User');
    }

    public function balance()
    {
        $credit = $this->where('action','credit')->sum('amount');
        $debit = $this->where('action','debit')->sum('amount');
        return $credit - $debit;
    }

    public static function credits() {
      return self::where('user_id',Auth::user()->id)->where('action','credit')->sum('amount');
    }

    public static function debits() {
      return self::where('user_id',Auth::user()->id)->where('action','debit')->sum('amount');
    }

    public function tree()
    {
        $this->belongsTo('App\Tree');
    }

    public static function bonus_following($user_id)
    {
        return self::where('user_id',$user_id)
                     ->where('action','credit')
                     ->where('type','invite')
                     ->sum('amount');
    }

    public static function bonus_login($user_id)
    {
        return self::where('user_id',$user_id)
                     ->where('action','credit')
                     ->where('type','login')
                     ->sum('amount');
    }

    public static function bonus_leveling($user_id)
    {
        return self::where('user_id',$user_id)
                     ->where('action','credit')
                     ->where('type','level')
                     ->sum('amount');
    }

    public static function bonus_pairing($user_id)
    {
        return self::where('user_id',$user_id)
                     ->where('action','credit')
                     ->where('type','pair')
                     ->sum('amount');
    }

    
}
