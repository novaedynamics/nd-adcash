<?php

return [
	// Login credit
	'credits' => [
        'login' => ['amount' => 1.00],
        'login-first' => ['amount' => 1.00],
		'login-second' => ['amount' => 1.00],
        'signup' => ['amount' => 2.00],
        'pairing' => ['amount' => 2.00],
        'leveling' => ['amount' => 8.00],
		'invite' => ['amount' => 2.00],
        'video' => ['amount' => 1.00]
	],
	'max' => [
		'login' => ['amount' => 30]
	],
    'limit' => [
        'login' => [
            'days' => 12,   // number of days
            'day' => 100,   // max amount per day
        ],
        'pairing' => [
            'day' => 30     // pairs per day
        ],
        'leveling' => 12       // 12 levels
    ],
	'currency' => array(
        'us' => '$',
        'ph' => 'PHP'
    ),
    'activation-code' => [
    	'price' => 15.00,
        'minimum' => 2          // minimum number of code purchase
    ],
    'wallet' => [
        'payout' => ['min' => 60]
    ],
    'description' => [
    	'bonus' => [
    		'pairing'               => 'Congratulations! You just received a new pairing bonus.',
            'login'                 => 'Congratulations! You just received a login bonus today.',
            'login-first'           => 'Congratulations! You just received a first level login bonus today.',
    		'login-second'          => 'Congratulations! You just received a second level login bonus today.',
    		'signup'                => 'Congratulations! You just received a signing bonus from AFTHONIA.',
            'leveling'              => 'Congratulations! You just received a new leveling bonus.',
            'invite'                => 'Congratulations! You just received a direct referral bonus.',
            'payout-code'           => 'Congratulations! For a successful code purchase.',
    		'video'                 => 'Congratulations! You just received a viewed video bonus',
    	]
    ],
    'referral' => ['default' => 'd1877342dbe3b34617e2ffc43ba26d40'],
    'payout' => [
        'options' => [],
        'amount' => []
    ],
    'levels' => [
        'paring' => 30,
        'leveling' => 12 // minus 1 so this is only level 12
    ],
    'withdrawal' => [
        'method' => ['(BTC) Bitcoin','(ETH) Ethereum','(XRP) Ripple','Coins.ph'],
        'denomination' => [60,120,220,320,420]
    ]

];