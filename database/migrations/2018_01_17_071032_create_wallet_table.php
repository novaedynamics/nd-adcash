<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWalletTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description')->nullable();
            $table->string('ip')->nullable();
            $table->decimal('amount', 6, 2)->default(0.00);
            $table->enum('action',array('debit','credit'));
            $table->enum('type',array('login','invite','pair','signup','level','payout'));
            $table->enum('status',array('Y','N','P','C','X'))->default('Y');
            $table->integer('user_id')->unsigned()->default(1);
            $table->integer('tree_id')->unsigned()->default(1)->nullable();
            $table->integer('from_tree_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallets');
    }
}