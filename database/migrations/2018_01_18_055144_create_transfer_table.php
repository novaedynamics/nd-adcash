<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfers', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            // $table->integer('code_id');
            $table->integer('user_id');
            $table->integer('from_id')->nullable();
            $table->integer('pin_id')->nullable();
            $table->integer('code_id')->nullable();
            $table->string('description')->nullable();
            $table->string('ip');
            $table->enum('type',['code','pin']);
            // $table->string('token',50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfer');
    }
}
