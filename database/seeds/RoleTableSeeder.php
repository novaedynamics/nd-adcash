<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_regular = new Role();
    	$role_regular->name = "Regular";
    	$role_regular->description = "A regular role";
    	$role_regular->save();

    	$role_premium = new Role();
    	$role_premium->name = "Premium";
    	$role_premium->description = "A premium role";
    	$role_premium->save();

    	$role_admin = new Role();
    	$role_admin->name = "Admin";
    	$role_admin->description = "An admin role";
    	$role_admin->save();

    	$role_ban = new Role();
    	$role_ban->name = "Ban";
    	$role_ban->description = "A ban role";
    	$role_ban->save();

    	$role_super_admin = new Role();
    	$role_super_admin->name = "Super Admin";
    	$role_super_admin->description = "A super admin role";
    	$role_super_admin->save();
    }
}
