<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\Tree;
use App\Code;
use App\Pin;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_premium = new User();
        $user_premium->name = 'Premium';
        $user_premium->email = 'premium@adcash.network';
        $user_premium->password = bcrypt('secret');
        $user_premium->avatar = 'avatars/'.rand(1,22).'.png';
        $user_premium->save();
        $user_premium->roles()->attach(Role::where('name','Premium')->first());
        
        $tree = new Tree();
        $tree->name = 'node_'.time();
        $tree->user_id = $user_premium->id;
        $tree->parent_id = null;
        $tree->save();
        sleep(1);


        // $user_regular = new User();
        // $user_regular->name = 'Regular';
        // $user_regular->email = 'regular@adcash.network';
        // $user_regular->password = bcrypt('secret');
        // $user_premium->avatar = 'avatars/'.rand(1,22).'.png';
        // $user_regular->save();
        // $user_regular->roles()->attach(Role::where('name','Regular')->first());

        // $tree = new Tree();
        // $tree->name = 'node_'.time();
        // $tree->user_id = $user_regular->id;
        // $tree->parent_id = null;
        // $tree->save();
        // sleep(1);

        $user_admin = new User();
        $user_admin->name = 'Admin';
        $user_admin->email = 'admin@adcash.network';
        $user_admin->password = bcrypt('secret');
        $user_premium->avatar = 'avatars/'.rand(1,22).'.png';
        $user_admin->save();
        $user_admin->roles()->attach(Role::where('name','Admin')->first());

        $tree = new Tree();
        $tree->name = 'node_'.time();
        $tree->user_id = $user_admin->id;
        $tree->parent_id = null;
        $tree->save();
        sleep(1);

        // $user_ban = new User();
        // $user_ban->name = 'Ban';
        // $user_ban->email = 'ban@adcash.network';
        // $user_ban->password = bcrypt('secret');
        // $user_premium->avatar = 'avatars/'.rand(1,22).'.png';
        // $user_ban->save();
        // $user_ban->roles()->attach(Role::where('name','Ban')->first());

        // $tree = new Tree();
        // $tree->name = 'node_'.time();
        // $tree->user_id = $user_ban->id;
        // $tree->parent_id = null;
        // $tree->save();
        // sleep(1);

        // $user_super_admin = new User();
        // $user_super_admin->name = 'Super Admin';
        // $user_super_admin->email = 'super@adcash.network';
        // $user_super_admin->password = bcrypt('secret');
        // $user_premium->avatar = 'avatars/'.rand(1,22).'.png';
        // $user_super_admin->save();
        // $user_super_admin->roles()->attach(Role::where('name','Super Admin')->first());

        // $tree = new Tree();
        // $tree->name = 'node_'.time();
        // $tree->user_id = $user_super_admin->id;
        // $tree->parent_id = null;
        // $tree->save();


        // for( $i = 0; $i < 100; $i++) {

        //     $user_regular = new User();
        //     $user_regular->name = 'Adcash'.($i+1);
        //     $user_regular->email = 'adcash'.($i+1).'@gmail.com';
        //     $user_regular->password = bcrypt('123456');
        //     $user_regular->avatar = 'avatars/'.rand(1,22).'.png';
        //     $user_regular->parent_id = 1;
        //     $user_regular->save();
        //     $user_regular->roles()->attach(Role::where('name','Regular')->first());

        //     $tree = new Tree();
        //     $tree->name = 'node_'.$i.time();
        //     $tree->user_id = $user_regular->id;
        //     $tree->parent_id = null;
        //     $tree->save();

        //     $code = new Code();
        //     $code->user_id = $user_regular->id;
        //     $code->save();

        //     $pin = new Pin();
        //     $pin->pin = rand(00000,99999);
        //     $pin->user_id = $user_regular->id;
        //     $pin->save();
        // }
        

    }
}
