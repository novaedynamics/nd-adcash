'use strict';

jQuery(document).ready(function($) {

	// Avatar Form
    var img_file;

    $('#form-change-avatar input[type="file"]').on('change', function(e) {
        img_file = e.target.files;
    });

    $('#form-change-avatar a.submit').on('click', function(e) {

        e.stopPropagation();
        e.preventDefault();
        
        var l = Ladda.create(this);
        var data = new FormData($('#form-change-avatar')[0]);
        l.start();

        setTimeout(function() {

            // var url = window.location.href.replace('edit','avatar');
            var url = '/api/account/avatar';

            $.ajax({
                url: url,
                data: data,
                dataType:'json',
                async:false,
                type:'post',
                processData: false,
                contentType: false,
                complete: function(res) {

                    l.stop();

                    if(res.responseJSON == 201) {
                        swal("Oops!", "Missing avatar file", "error");
                        l.stop();
                        return;
                    } else {
                        // $('.img-circle').attr('src','/uploads/' + res.responseJSON.file);
                        $('.nav-avatar').attr('src','/uploads/' + res.responseJSON.file);
                        $('#form-change-avatar input[type="file"]').attr('value','');
                        swal("Success!", "Your avatar has been updated.", "success");

                        return;
                    }
                    
                }
            });
        }, 1000);

    });

});