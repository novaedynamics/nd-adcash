'use strict';

jQuery(document).ready(function($) {
	$('.btn-profile-save').on('click', function(e) {
		
        e.stopPropagation();
        e.preventDefault();

        var l = Ladda.create(this);                
        var data = $('#form-profile').serializeArray();
        console.log(data);
        l.start();

        setTimeout(function() {
            $.ajax({
                url: '/api/account',
                data: data,
                dataType:'json',
                async:false,
                type:'post',
                // processData: false,
                // contentType: false,
                statusCode: {
                    200: function() {
                        swal("Success!", "Your account has been updated.", "success");
                    }
                },
                complete: function() {
                    l.stop();
                }
            });
        }, 1000);

	});

    $('.btn-change-password').on('click', function(e) {
        
        e.stopPropagation();
        e.preventDefault();

        var l = Ladda.create(this);                
        var data = $('#form-change-password').serializeArray();
        l.start();

        setTimeout(function() {
            $.ajax({
                url: '/api/account/change-password',
                data: data,
                dataType:'json',
                async:false,
                type:'post',
                // processData: false,
                // contentType: false,
                statusCode: {
                    200: function() {
                        swal("Success!", "Your account has been updated.", "success");
                    }
                },
                complete: function() {
                    l.stop();
                }
            });
        }, 1000);

    });

    // $('#copy-referral').on('click', function(e) {
            
        


    // });



});