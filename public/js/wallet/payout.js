'use strict';

jQuery(document).ready(function($) {

	App.adcash = new Object();
	App.adcash.withdraw = new Object();

	$('.wmethod').html($('select[name="payout-type"]').val());
	$('.wamount').html($('select[name="amount"]').val());

	$('.wmobile').hide();

	$('select[name="amount"]').on('change', function(e) { 
		App.adcash.withdraw.amount = $(this).val();
		$('.wamount').html(App.adcash.withdraw.amount);
	});

	$('select[name="payout-type"]').on('change', function(e) { 
		App.adcash.withdraw.method = $(this).val();
		$('.wwallet').show();
		$('.waddress').show();
		$('.wmobile').show();
		$('.wmethod strong').html(App.adcash.withdraw.method);
		if($.inArray( App.adcash.withdraw.method, ['Coins.ph'])) {
			// $('.wwallet').hide(); $('.waddress').hide();
			$('.wmobile').hide();
		}
	});

	$('input[name="waddress"]').on('input', function(e) { 
		App.adcash.withdraw.waddress = $(this).val();
		$('td.waddress').html(App.adcash.withdraw.waddress);
	});

	$('input[name="wmobile"]').on('input', function(e) { 
		App.adcash.withdraw.wmobile = $(this).val();
		$('td.wmobile').html(App.adcash.withdraw.wmobile);
	});

	$('textarea[name="wmessage"]').on('input', function(e) { 
		App.adcash.withdraw.message = $(this).val();
		$('td.wmessage').html(App.adcash.withdraw.message);
	});

	$('[data-toggle=confirmation]').confirmation({
	  rootSelector: '[data-toggle=confirmation]',
		  onConfirm: function() {
	    alert('You choosed ');
	  },
	  onCancel: function() {
	    alert('You didn\'t choose');
	  }
	});

	$('.btn-complete-payment').on('click', function(e) {

		App.adcash.withdraw._token = $('input[name="_token"]').val();
		App.adcash.withdraw.userid = $('input[name="userid"]').val();
		App.adcash.withdraw.amount = $('select[name="amount"]').val();
		App.adcash.withdraw.method = $('select[name="payout-type"]').val();
		App.adcash.withdraw.waddress = $('input[name="waddress"]').val();
		App.adcash.withdraw.wmobile = $('input[name="wmobile"]').val();
		App.adcash.withdraw.message = $('textarea[name="wmessage"]').val();

		var l = Ladda.create(this);
        
        l.start();

        setTimeout(function() {

        	$('#basic').modal('toggle');

        	// swal("Success!", "Your account has been updated.", "success");
        	// l.stop();

            $.ajax({
                url: '/api/finance/withdraw',
                data: App.adcash.withdraw,
                dataType:'json',
                async:false,
                type:'post',
                complete: function(d) {

                	let response = JSON.parse(d.responseText);

                	if(response.success){

						swal({
						  title: "Success",
						  text: "We are now processing your payout request.",
						  type: "success",
						  confirmButtonClass: "btn-success",
						  confirmButtonText: "OK",
						  // cancelButtonText: "No, cancel plx!",
						  closeOnConfirm: true,
						  // closeOnCancel: false
						},
						function(isConfirm) {
						  	// alert('test');
						});

                	} else {
                		// swal("Opps!", "", "error");

                		var text;

                		if(typeof response.message == 'undefined') 
                			text = "It seems that there is a problem with your payout request. \n Please try again later.";
                		else
                			text = "You need at least $250.00 as your first payout request.";

                		swal({
						  title: "Opps!",
						  text: text,
						  type: "error",
						  confirmButtonClass: "btn-error",
						  confirmButtonText: "OK",
						  // cancelButtonText: "No, cancel plx!",
						  closeOnConfirm: true,
						  // closeOnCancel: false
						},
						function(isConfirm) {
						  	// alert('test');
						});
                	}

                	l.stop();
                }
            });

        }, 1000);

	});	

});