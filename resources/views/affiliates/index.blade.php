@extends('layouts.dash')

@section('title','Ads &amp; Affiliates')
@section('slogan','')

@section('content')

<div class="portfolio-content portfolio-3">
<div class="clearfix">
    <div id="js-filters-lightbox-gallery1" class="cbp-l-filters-dropdown cbp-l-filters-dropdown-floated">
        <!-- <div class="cbp-l-filters-dropdownWrap border-grey-salsa">
            <div class="cbp-l-filters-dropdownHeader uppercase">Sort Gallery</div>
            <div class="cbp-l-filters-dropdownList">
                <div data-filter="*" class="cbp-filter-item-active cbp-filter-item uppercase"> All (
                    <div class="cbp-filter-counter"></div> items) </div>
                <div data-filter=".identity" class="cbp-filter-item uppercase"> Identity (
                    <div class="cbp-filter-counter"></div> items) </div>
                <div data-filter=".web-design" class="cbp-filter-item uppercase"> Web Design (
                    <div class="cbp-filter-counter"></div> items) </div>
                <div data-filter=".print" class="cbp-filter-item uppercase"> Print (
                    <div class="cbp-filter-counter"></div> items) </div>
            </div>
        </div> -->
    </div>
    <div id="js-filters-lightbox-gallery2" class="cbp-l-filters-button cbp-l-filters-left">
        <div data-filter="*" class="cbp-filter-item-active cbp-filter-item btn blue btn-outline uppercase">All</div>
        <div data-filter=".crypto" class="cbp-filter-item btn blue btn-outline uppercase">Crypto Currency</div>
        <!-- <div data-filter=".logos" class="cbp-filter-item btn blue btn-outline uppercase">Logo</div>
        <div data-filter=".motion" class="cbp-filter-item btn blue btn-outline uppercase">Motion</div> -->
    </div>
</div>
<div id="js-grid-lightbox-gallery" class="cbp">
    <div class="cbp-item web-design crypto print motion">
        <a href="ajax/cubefolio/godaddy-en.html" class="cbp-caption cbp-singlePageInline" data-title="World Clock Widget<br>by Paul Flavius Nechita" rel="nofollow">
            <div class="cbp-caption-defaultWrap">
                <img src="uploads/images/GoDaddy-300x250.gif" alt=""> </div>
            <div class="cbp-caption-activeWrap">
                <div class="cbp-l-caption-alignLeft">
                    <div class="cbp-l-caption-body">
                        <div class="cbp-l-caption-title">Go Daddy</div>
                        <div class="cbp-l-caption-desc">International</div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="cbp-item web-design crypto print motion">
        <a href="ajax/cubefolio/iqoption-indo.html" class="cbp-caption cbp-singlePageInline" data-title="World Clock Widget<br>by Paul Flavius Nechita" rel="nofollow">
            <div class="cbp-caption-defaultWrap">
                <img src="uploads/images/20123_300x250.gif" alt=""> </div>
            <div class="cbp-caption-activeWrap">
                <div class="cbp-l-caption-alignLeft">
                    <div class="cbp-l-caption-body">
                        <div class="cbp-l-caption-title">IQ Option</div>
                        <div class="cbp-l-caption-desc">Bahasa</div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="cbp-item web-design crypto print motion">
        <a href="ajax/cubefolio/iqoption-en.html" class="cbp-caption cbp-singlePageInline" data-title="World Clock Widget<br>by Paul Flavius Nechita" rel="nofollow">
            <div class="cbp-caption-defaultWrap">
                <img src="uploads/images/20099_300x250.gif" alt=""> </div>
            <div class="cbp-caption-activeWrap">
                <div class="cbp-l-caption-alignLeft">
                    <div class="cbp-l-caption-body">
                        <div class="cbp-l-caption-title">IQ Option</div>
                        <div class="cbp-l-caption-desc">International</div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="cbp-item web-design crypto print motion">
        <a href="ajax/cubefolio/etoro-en.html" class="cbp-caption cbp-singlePageInline" data-title="Etoro Affiliate<br> International" rel="nofollow">
            <div class="cbp-caption-defaultWrap">
                <img src="//partners.etoro.com/B10248_A11511_TGet.aspx" alt=""> </div>
            <div class="cbp-caption-activeWrap">
                <div class="cbp-l-caption-alignLeft">
                    <div class="cbp-l-caption-body">
                        <div class="cbp-l-caption-title">Etoro</div>
                        <div class="cbp-l-caption-desc">International</div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="cbp-item web-design crypto print motion">
        <a href="ajax/cubefolio/binance-en.html" class="cbp-caption cbp-singlePageInline" data-title="Etoro Affiliate<br> International" rel="nofollow">
            <div class="cbp-caption-defaultWrap">
                <img src="uploads/images/DG2tJUQV0AAibzv.jpg" alt=""> </div>
            <div class="cbp-caption-activeWrap">
                <div class="cbp-l-caption-alignLeft">
                    <div class="cbp-l-caption-body">
                        <div class="cbp-l-caption-title">BINANCE Exchange</div>
                        <div class="cbp-l-caption-desc">International</div>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
<!-- <div id="js-loadMore-lightbox-gallery" class="cbp-l-loadMore-button">
    <a href="../assets/global/plugins/cubeportfolio/ajax/loadMore3.html" class="cbp-l-loadMore-link btn grey-mint btn-outline" rel="nofollow">
        <span class="cbp-l-loadMore-defaultText">LOAD MORE</span>
        <span class="cbp-l-loadMore-loadingText">LOADING...</span>
        <span class="cbp-l-loadMore-noMoreLoading">NO MORE WORKS</span>
    </a>
</div> -->
</div>

@endsection

@section('page-plugin-css')
<link href="/assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/ladda/ladda-themeless.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/cubeportfolio/css/cubeportfolio.css" rel="stylesheet" type="text/css" />
<link href="/assets/pages/css/portfolio.min.css" rel="stylesheet" type="text/css" />
<style>
@media( max-width: 668px ) {
    .videoWrapper {
        position: relative;
        padding-bottom: 56.25%; /* 16:9 */
        padding-top: 25px;
        height: 0;
    }
    .videoWrapper iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
}
</style>
@endsection

@section('page-plugin')
<script src="/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
@endsection

@section('page-script')
<script src="/assets/pages/scripts/portfolio-3.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/ladda/spin.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/ladda/ladda.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script>
@endsection