@extends('layouts.login')

@section('title','Ad Cash Network')

@section('content')
        <div class="user-login-5">
            <div class="row bs-reset">
                <div class="col-md-6 login-container bs-reset">
                    <!-- <img class="login-logo login-6" src="/assets/pages/img/login/login-invert.png" /> -->
                    <div class="login-content">
                        <h1>{{ config('app.name') }} &ndash; Member Login</h1>
                        <p>Join the fastest growing affiliate network in the world and watch your profits surge!</p>

                        <form class="form-horizontal login-form" method="POST" action="{{ route('login') }}">
                        
                            {{ csrf_field() }}

                            @if ($errors->any()) 
                                <div class="alert alert-danger">
                                    <button class="close" data-close="alert"></button>
                                    @foreach($errors->all() as $error)
                                        <span class="help-block">
                                            <span style="color: #e73d4a;">{{ $error }}</span>
                                        </span>
                                    @endforeach
                                </div>
                            @endif
                            
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                <span>Enter any username and password. </span>
                            </div>

                            <div class="row">
                                <div class="col-xs-6">                                    
                                    <input id="email" type="text" class="form-control form-control-solid placeholder-no-fix form-group" placeholder="Enter your email address" name="email" value="{{ old('email') }}" required autofocus autocomplete="off">
                                </div>
                                <div class="col-xs-6">                                    
                                    <input id="password" type="password" class="form-control" name="password" autocomplete="off" required placeholder="Enter your password"> </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="rememberme mt-checkbox mt-checkbox-outline">
                                        <!-- <input type="checkbox" name="remember" value="1" /> Remember me
                                        <span></span> -->
                                    </label>
                                </div>
                                <div class="col-sm-8 text-right">
                                    <div class="forgot-password">
                                        <a href="{{ route('register') }}">Create an account</a> | <a href="{{ route('password.request') }}" >Forgot Password?</a>
                                    </div>
                                    <button class="btn blue" type="submit">Sign In</button>
                                </div>
                            </div>
                        </form>

                    </div>
                    <div class="login-footer">
                        <div class="row bs-reset">
                            <div class="col-xs-5 bs-reset">
                                <!-- <ul class="login-social">
                                    <li>
                                        <a href="https://facebook.com/{{config('app.facebook')}}">
                                            <i class="icon-social-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://twitter.com/{{config('app.twitter')}}">
                                            <i class="icon-social-twitter"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://dribbble.com/{{config('app.dribbble')}}">
                                            <i class="icon-social-dribbble"></i>
                                        </a>
                                    </li>
                                </ul> -->
                            </div>
                            <div class="col-xs-7 bs-reset">
                                <div class="login-copyright text-right">
                                    <p>Copyright &copy; {{ config('app.name') }} <?php echo date('Y');?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 bs-reset">
                    <div class="login-bg"> </div>
                </div>
            </div>
        </div>

@endsection