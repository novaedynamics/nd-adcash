@extends('layouts.dash')

@section('title','Activation Codes')

@section('content')

@if (session('success'))
<div class="note note-success">
    <h4 class="block">{{session('success')}}</h4>
    <!-- <p> Duis mollis, est non commodo luctus, nisi erat mattis consectetur purus sit amet porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. </p> -->
</div>
@endif

@if (session('confirm'))
<div class="note note-warning">
    <h4 class="block">{{session('confirm')}}</h4>
    <p>A confirmation link has been dispatched to your email. Click the link to complete the request!</p>
</div>
@endif


<div class="row">
        <div class="col-md-12">
            <!-- <div class="note note-success">
                <p> Please try to re-size your browser window in order to see the tables in responsive mode. </p>
            </div> -->
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <!-- <div class="caption">
                        <i class="fa fa-cogs"></i>Responsive Flip Scroll Tables </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div> -->
                </div>
                <div class="portlet-body flip-scroll">
                    <table class="table table-bordered table-striped table-condensed flip-content code">
                        <thead class="flip-content">
                            <tr>
                                <th width="20%"> Purchased Date </th>
                                <th width="60%"> Code </th>                                
                                <th width="20%"> Status </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($codes as $code)
                            <tr>
                                <td>{{ $code->created_at }}</td>
                                <td>{{ $code->code }}</td>
                                <td>{{ $code->status }}</td>
                            </tr>    
                            @endforeach                        
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="portlet box green">
                <div class="portlet-title">
                    <!-- <div class="caption">
                        <i class="fa fa-cogs"></i>Responsive Flip Scroll Tables </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div> -->
                </div>
                <div class="portlet-body flip-scroll">
                    <table class="table table-bordered table-striped table-condensed flip-content code-history">
                        <thead class="flip-content">
                            <tr>
                                <th width="20%"> Transfered Date </th>
                                <th width="80%"> Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($transfers as $t)
                            <tr>
                                <td>{{ $t->created_at }}</td>
                                <td>{{ $t->description }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="form-actions">
                <a href="{{ route('purchase-codes') }}" class="btn blue">Purchase</a>
                <a href="{{ route('transfer-codes') }}" class="btn blue">Transfer</a>
                @if(in_array(Auth::user()->roles()->first()->name,['Admin','Super Admin']))
                <a href="{{ route('generate-codes') }}" class="btn blue">Generate Code</a>
                @endif
            </div>
            <!-- END SAMPLE TABLE PORTLET-->                           
            
        </div>
    </div>

@endsection

@section('page-plugin-css')
<link href="/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
@endsection

@section('page-plugin')
<script src="/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
@endsection

@section('page-script')
<script src="/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
<script>
    $('table.code').DataTable();
    $('table.code-history').DataTable();
</script>
@endsection