@extends('layouts.dash')

@section('title','Dashboard')

@section('content')

<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-sharp">
                        <span data-counter="counterup" data-value="{{ $wallet_total }}">0</span>
                        <small class="font-green-sharp">{{ config('app.currency.us') }}</small>
                    </h3>
                    <small>WALLET BALANCE</small>
                </div>
                <div class="icon">
                    <i class="fa fa-podcast"></i>
                </div>
            </div>
            
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-red-haze">
                        <span data-counter="counterup" data-value="{{ $wallet_bonus }}">0</span>
                        <small class="font-red-haze">{{ config('app.currency.us') }}</small>
                    </h3>
                    <small>TOTAL BONUS</small>
                </div>
                <div class="icon">
                    <i class="fa fa-sign-in"></i>
                </div>
            </div>
            
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-blue-sharp">
                        <span data-counter="counterup" data-value="{{ $wallet_payout }}">0</span>
                        <small class="font-blue-sharp">{{ config('app.currency.us') }}</small>
                    </h3>
                    <small>APPROVED PAYOUT</small>
                </div>
                <div class="icon">
                    <i class="fa fa-level-up"></i>
                </div>
            </div>
            
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-purple-soft">
                        <span data-counter="counterup" data-value="{{ $wallet_pending_payout }}">0</span>
                        <small class="font-purple-sharp">{{ config('app.currency.us') }}</small>
                    </h3>
                    <small>PAYOUT REQUEST</small>
                </div>
                <div class="icon">
                    <i class="icon-users"></i>
                </div>
            </div>
            
        </div>
    </div>
</div>

                    <div class="row">
                        <div class="col-lg-6 col-xs-12 col-sm-12">
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <span class="caption-subject bold uppercase font-dark">Direct Referrals</span>
                                        <span class="caption-helper">distance stats...</span>
                                    </div>
                                    <!-- <div class="actions">
                                        <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                            <i class="icon-cloud-upload"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                            <i class="icon-wrench"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                            <i class="icon-trash"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#"> </a>
                                    </div> -->
                                </div>
                                <div class="portlet-body">
                                    <div id="chart_signups" class="CSSAnimationChart"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xs-12 col-sm-12">
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption ">
                                        <span class="caption-subject font-dark bold uppercase">Finance</span>
                                        <span class="caption-helper">distance stats...</span>
                                    </div>
                                    <!-- <div class="actions">
                                        <a href="#" class="btn btn-circle green btn-outline btn-sm">
                                            <i class="fa fa-pencil"></i> Export </a>
                                        <a href="#" class="btn btn-circle green btn-outline btn-sm">
                                            <i class="fa fa-print"></i> Print </a>
                                    </div> -->
                                </div>
                                <div class="portlet-body">
                                    <div id="chart_finance" class="CSSAnimationChart"></div>
                                    <!-- <div id="chartdiv1" class="CSSAnimationChart"></div> -->
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-lg-6 col-xs-12 col-sm-12 tabbable-line">
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption caption-md">
                                        <i class="icon-bar-chart font-dark hide"></i>
                                        <span class="caption-subject font-dark bold uppercase">Top Performers</span>
                                        <!-- <span class="caption-helper">weekly stats...</span> -->
                                    </div>
                                    <div class="actions">
                                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                                            <!-- <label class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm active">
                                                <input type="radio" name="options" class="toggle" id="option1">Today</label>
                                            <label class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm">
                                                <input type="radio" name="options" class="toggle" id="option2">Week</label>
                                            <label class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm">
                                                <input type="radio" name="options" class="toggle" id="option2">Month</label> -->
                                                <a href="#tab_top_today" data-toggle="tab" class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm">
                                                Today</a>
                                            <a href="#tab_top_week" data-toggle="tab" class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm">
                                                Week</a>
                                            <a href="#tab_top_month" data-toggle="tab" class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm">
                                                Month</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body tab-content">
                                    <div class="tab-pane" id="tab_top_today">
                                        <div class="row number-stats margin-bottom-30">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="stat-left">
                                                    <div class="stat-chart">
                                                        <!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
                                                        <div id="sparkline_bar_today_1"></div>
                                                    </div>
                                                    <!-- <div class="stat-number">
                                                        <div class="title"> Total </div>
                                                        <div class="number">  </div>
                                                    </div> -->
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="stat-right">
                                                    <!-- <div class="stat-chart"> -->
                                                        <!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
                                                        <!-- <div id="sparkline_bar_today_2"></div> -->
                                                    <!-- </div> -->
                                                    <div class="stat-number">
                                                        <div class="title"> New Today </div>
                                                        <div class="number"> {{ $count_active_users_today }} </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table-scrollable table-scrollable-borderless">
                                            <table class="table table-hover table-light">
                                                <thead>
                                                    <tr class="uppercase">
                                                        <th colspan="2"> MEMBER </th>
                                                        <th> Earnings </th>
                                                                                                       
                                                    </tr>
                                                </thead>

                                                @foreach($top_today as $today)
                                                    <tr>
                                                        <td class="fit">
                                                            <img class="user-pic rounded" src="uploads/{{ $today->user->avatar }}"> </td>
                                                        <td>
                                                            <a href="javascript:;" class="primary-link">{{ $today->user->name }}</a>
                                                        </td>
                                                        <td> {{ config('adcash.currency.us') }}{{ $today->earnings }} </td>
                                                        
                                                    </tr>
                                                @endforeach
                                                
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane active" id="tab_top_week">
                                        
                                        <div class="row number-stats margin-bottom-30">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="stat-left">
                                                    <div class="stat-chart">
                                                        <!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
                                                        <!-- <div id="sparkline_bar_week_1"></div> -->
                                                        <div id="sparkline_bar_week_2"></div>
                                                    </div>
                                                    <!-- <div class="stat-number">
                                                        <div class="title"> Total </div>
                                                        <div class="number"> {{ $users_count }} </div>
                                                    </div> -->
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="stat-right">
                                                    <div class="stat-chart">
                                                        <!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
                                                        <!-- <div id="sparkline_bar_week_2"></div> -->
                                                    </div>
                                                    <div class="stat-number">
                                                        <div class="title"> New This Week</div>
                                                        <div class="number"> {{ $count_active_users_week }} </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table-scrollable table-scrollable-borderless">
                                            <table class="table table-hover table-light">
                                                <thead>
                                                    <tr class="uppercase">
                                                        <th colspan="2"> MEMBER </th>
                                                        <th> Earnings </th>
                                                                                                       
                                                    </tr>
                                                </thead>

                                                @foreach($top_week as $week)
                                                    <tr>
                                                        <td class="fit">
                                                            <img class="user-pic rounded" src="uploads/{{ $week->user->avatar }}"> </td>
                                                        <td>
                                                            <a href="javascript:;" class="primary-link">{{ $week->user->name }}</a>
                                                        </td>
                                                        <td> {{ config('adcash.currency.us') }}{{ $week->earnings }} </td>
                                                        
                                                    </tr>
                                                @endforeach
                                                
                                            </table>
                                        </div>

                                    </div>
                                    <div class="tab-pane" id="tab_top_month">
                                        <div class="row number-stats margin-bottom-30">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="stat-left">
                                                    <div class="stat-chart">
                                                        <!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
                                                        <div id="sparkline_bar_month_1"></div>
                                                    </div>
                                                    <!-- <div class="stat-number">
                                                        <div class="title"> Total </div>
                                                        <div class="number"> {{ $users_count }} </div>
                                                    </div> -->
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="stat-right">
                                                    <div class="stat-chart">
                                                        <!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
                                                        <!-- <div id="sparkline_bar_month_2"></div> -->
                                                    </div>
                                                    <div class="stat-number">
                                                        <div class="title"> New This Month</div>
                                                        <div class="number"> {{ $count_active_users_month }} </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table-scrollable table-scrollable-borderless">
                                            <table class="table table-hover table-light">
                                                <thead>
                                                    <tr class="uppercase">
                                                        <th colspan="2"> MEMBER </th>
                                                        <th> Earnings </th>
                                                    </tr>
                                                </thead>

                                                @foreach($top_month as $month)
                                                    <tr>
                                                        <td class="fit">
                                                            <img class="user-pic rounded" src="uploads/{{ $month->user->avatar }}"> </td>
                                                        <td>
                                                            <a href="javascript:;" class="primary-link">{{ $month->user->name }}</a>
                                                        </td>
                                                        <td> {{ config('adcash.currency.us') }}{{ $month->earnings }} </td>
                                                    </tr>
                                                @endforeach
                                                
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xs-12 col-sm-12 tabbable-line">
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption caption-md">
                                        <i class="icon-bar-chart font-dark hide"></i>
                                        <span class="caption-subject font-dark bold uppercase">Top Code Conversions</span>
                                    </div>
                                    <div class="actions">
                                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                                                <a href="#tab_conversion_today" data-toggle="tab" class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm">
                                                Today</a>
                                            <a href="#tab_conversion_week" data-toggle="tab" class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm">
                                                Week</a>
                                            <a href="#tab_conversion_month" data-toggle="tab" class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm">
                                                Month</a>
                                            <a href="#tab_conversion_month6" data-toggle="tab" class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm">
                                                6-Month</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body tab-content">
                                    <div class="tab-pane" id="tab_conversion_today">
                                        <div class="row number-stats margin-bottom-30">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="stat-left">
                                                    <div class="stat-chart">
                                                        <!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
                                                        <div id="sparkline_bar_con_today_1"></div>
                                                    </div>
                                                    <!-- <div class="stat-number">
                                                        <div class="title"> Total </div>
                                                        <div class="number">  </div>
                                                    </div> -->
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="stat-right">
                                                    <!-- <div class="stat-chart"> -->
                                                        <!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
                                                        <!-- <div id="sparkline_bar_con_today_2"></div> -->
                                                    <!-- </div> -->
                                                    <div class="stat-number">
                                                        <div class="title"> Conversions Today </div>
                                                        <div class="number"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table-scrollable table-scrollable-borderless">
                                            <table class="table table-hover table-light">
                                                <thead>
                                                    <tr class="uppercase">
                                                        <th colspan="2"> MEMBER </th>
                                                        <th> Conversions </th>
                                                                                                       
                                                    </tr>
                                                </thead>

                                                @foreach($top_converter_day as $today)
                                                    <tr>
                                                        <td class="fit">
                                                            <img class="user-pic rounded" src="uploads/{{ $today->user->avatar }}"> </td>
                                                        <td>
                                                            <a href="javascript:;" class="primary-link">{{ $today->user->name }}</a>
                                                        </td>
                                                        <td> {{ $today->total }} </td>
                                                        
                                                    </tr>
                                                @endforeach
                                                
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_conversion_week">
                                        
                                        <div class="row number-stats margin-bottom-30">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="stat-left">
                                                    <div class="stat-chart">
                                                        <!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
                                                        <!-- <div id="sparkline_bar_con_week_1"></div> -->
                                                        <div id="sparkline_bar_con_week_2"></div>
                                                    </div>
                                                    <!-- <div class="stat-number">
                                                        <div class="title"> Total </div>
                                                        <div class="number"> {{ $users_count }} </div>
                                                    </div> -->
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="stat-right">
                                                    <div class="stat-chart">
                                                        <!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
                                                        <!-- <div id="sparkline_bar_con_week_2"></div> -->
                                                    </div>
                                                    <div class="stat-number">
                                                        <div class="title"> Conversions This Week</div>
                                                        <div class="number"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table-scrollable table-scrollable-borderless">
                                            <table class="table table-hover table-light">
                                                <thead>
                                                    <tr class="uppercase">
                                                        <th colspan="2"> MEMBER </th>
                                                        <th> Conversions </th>
                                                                                                       
                                                    </tr>
                                                </thead>

                                                @foreach($top_converter_week as $week)
                                                    <tr>
                                                        <td class="fit">
                                                            <img class="user-pic rounded" src="uploads/{{ $week->user->avatar }}"> </td>
                                                        <td>
                                                            <a href="javascript:;" class="primary-link">{{ $week->user->name }}</a>
                                                        </td>
                                                        <td> {{ $week->total }} </td>                           
                                                    </tr>
                                                @endforeach
                                                
                                            </table>
                                        </div>

                                    </div>
                                    <div class="tab-pane active" id="tab_conversion_month">
                                        <div class="row number-stats margin-bottom-30">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="stat-left">
                                                    <div class="stat-chart">
                                                        <!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
                                                        <div id="sparkline_bar_con_month"></div>
                                                    </div>
                                                    <!-- <div class="stat-number">
                                                        <div class="title"> Total </div>
                                                        <div class="number"> {{ $users_count }} </div>
                                                    </div> -->
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="stat-right">
                                                    <div class="stat-chart">
                                                        <!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
                                                        <!-- <div id="sparkline_bar_con_month_2"></div> -->
                                                    </div>
                                                    <div class="stat-number">
                                                        <div class="title"> Conversions This Month</div>
                                                        <div class="number">  </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table-scrollable table-scrollable-borderless">
                                            <table class="table table-hover table-light">
                                                <thead>
                                                    <tr class="uppercase">
                                                        <th colspan="2"> MEMBER </th>
                                                        <th> Conversions </th>
                                                    </tr>
                                                </thead>

                                                @foreach($top_converter_month as $month)
                                                    <tr>
                                                        <td class="fit">
                                                            <img class="user-pic rounded" src="uploads/{{ $month->user->avatar }}"> </td>
                                                        <td>
                                                            <a href="javascript:;" class="primary-link">{{ $month->user->name }}</a>
                                                        </td>
                                                        <td> {{ $month->total }} </td>
                                                    </tr>
                                                @endforeach
                                                
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_conversion_month6">
                                        <div class="row number-stats margin-bottom-30">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="stat-left">
                                                    <div class="stat-chart">
                                                        <!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
                                                        <div id="sparkline_bar_con_month6"></div>
                                                    </div>
                                                    <!-- <div class="stat-number">
                                                        <div class="title"> Total </div>
                                                        <div class="number">  </div>
                                                    </div> -->
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="stat-right">
                                                    <!-- <div class="stat-chart"> -->
                                                        <!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
                                                        <!-- <div id="sparkline_bar_con_today_2"></div> -->
                                                    <!-- </div> -->
                                                    <div class="stat-number">
                                                        <div class="title"> Conversions Last 6 Months </div>
                                                        <div class="number"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table-scrollable table-scrollable-borderless">
                                            <table class="table table-hover table-light">
                                                <thead>
                                                    <tr class="uppercase">
                                                        <th colspan="2"> MEMBER </th>
                                                        <th> Conversions </th>
                                                                                                       
                                                    </tr>
                                                </thead>

                                                @foreach($top_converter_range as $range)
                                                    <tr>
                                                        <td class="fit">
                                                            <img class="user-pic rounded" src="uploads/{{ $range->user->avatar }}"> </td>
                                                        <td>
                                                            <a href="javascript:;" class="primary-link">{{ $range->user->name }}</a>
                                                        </td>
                                                        <td> {{ $range->total }} </td>
                                                        
                                                    </tr>
                                                @endforeach
                                                
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-xs-12 col-sm-12">
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-bubble font-dark hide"></i>
                                        <span class="caption-subject font-hide bold uppercase">Recent Users</span>
                                    </div>
                                    
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        @foreach($recent_users as $user)
                                        <div class="col-md-4">
                                            <!--begin: widget 1-1 -->
                                            <div class="mt-widget-1">
                                                
                                                <div class="mt-img">
                                                    <img src="uploads/{{ $user->avatar }}" width="80" height="80"> </div>
                                                <div class="mt-body">
                                                    <h3 class="mt-username">{{ $user->name }}</h3>
                                                    <p class="mt-user-title">{{ $user->country ? $user->country : 'Location' }}</p>
                                                </div>
                                            </div>
                                            <!--end: widget 1-1 -->
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>


@endsection

@section('page-plugin-css')
    <link href="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
@endsection

@section('page-plugin')
    <script src="assets/global/plugins/moment.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
    <script src="assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
    <script src="assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
    <script src="assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
    <script src="assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
    <script src="assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
    <script src="assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
    <script src="assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
    <script src="assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
    <script src="assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
    <script src="assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
    <script src="assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
@endsection

@section('page-script')
<script src="assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
<script type="text/javascript">

var chart_signups_data = [
                {date:"2012-01-05",signups:480,townName:"Miami",townName2:"Miami",townSize:10,active:25.83,inactive:501},
                {date:"2012-01-06",signups:386,townName:"Tallahassee",townSize:7,active:30.46,inactive:443},
                {date:"2012-01-07",signups:348,townName:"New Orleans",townSize:10,active:29.94,inactive:405},
                {date:"2012-01-08",signups:238,townName:"Houston",townName2:"Houston",townSize:16,active:29.76,inactive:309},
                {date:"2012-01-09",signups:218,townName:"Dalas",townSize:17,active:32.8,inactive:287},
                {date:"2012-01-10",signups:349,townName:"Oklahoma City",townSize:11,active:35.49,inactive:485},
                {date:"2012-01-11",signups:603,townName:"Kansas City",townSize:10,active:39.1,inactive:890},
                {date:"2012-01-12",signups:534,townName:"Denver",townName2:"Denver",townSize:18,active:39.74,inactive:810},
                {date:"2012-01-13",townName:"Salt Lake City",townSize:12,signups:425,inactive:670,active:40.75,alpha:.4},
                {date:"2012-01-14",active:36.1,inactive:470,townName:"Las Vegas",townName2:"Las Vegas",bulletClass:"lastBullet"},
                {date:"2012-01-15"}
                ];

    $.get('/api/finance/chart/earnings', function(d) {
        chart_finance(d);
    });

    $.get('/api/finance/chart/signups', function(d) {
        App.signups = d;
        if(App.signups.length == 0)
            chart_signups(chart_signups_data);
        else
            chart_signups(d);
    });

    function chart_finance(data) {

        AmCharts.makeChart("chart_finance", {
                type: "serial",
                theme: 'light',
                dataProvider: data,
                categoryField: "date",
                startDuration: 1,

                categoryAxis: {
                    gridPosition: "start"
                },
                valueAxes: [{
                    title: "Million USD"
                }],
                graphs: [{
                    type: "column",
                    title: "Earnings",
                    valueField: "earnings",
                    lineAlpha: 0,
                    fillAlphas: 0.8,
                    balloonText: "[[title]] in [[category]]:<b>[[value]]</b>"
                }, {
                    type: "line",
                    title: "Payout",
                    valueField: "payout",
                    lineThickness: 2,
                    fillAlphas: 0,
                    bullet: "round",
                    balloonText: "[[title]] in [[category]]:<b>[[value]]</b>"
                }],
                legend: {
                    useGraphSettings: true
                }

            });

    }

    var chart_signups = function(data)
    {
        AmCharts.makeChart("chart_signups",{
        type:"serial",
        fontSize:12,
        fontFamily:"Open Sans",
        dataDateFormat:"YYYY-MM-DD",
        dataProvider: data,
        addClassNames:!0,
        startDuration:1,
        color:"#6c7b88",
        marginLeft:0,
        categoryField:"date",
        categoryAxis:{
            parseDates:!0,
            minPeriod:"DD",
            autoGridCount:!1,
            gridCount:50,
            gridAlpha:.1,
            gridColor:"#FFFFFF",
            axisColor:"#555555",
            dateFormats:[
                {period:"DD",format:"DD"},
                {period:"WW",format:"MMM DD"},
                {period:"MM",format:"MMM"},
                {period:"YYYY",format:"YYYY"}]},
                valueAxes:[
                    {id:"a1",title:"signups",gridAlpha:0,axisAlpha:0},
                    {id:"a2",position:"right",gridAlpha:0,axisAlpha:0,labelsEnabled:!1},
                    {id:"a3",title:"inactive",position:"right",gridAlpha:0,axisAlpha:0,inside:!0}
                ],
                graphs:[
                    {
                        id:"g1",
                        valueField:"signups",
                        title:"signups",
                        type:"column",
                        fillAlphas:.7,
                        valueAxis:"a1",
                        balloonText:"[[value]] signups",
                        legendValueText:"[[value]] signups",
                        legendPeriodValueText:"total: [[value.sum]] signups",
                        lineColor:"#08a3cc",
                        alphaField:"alpha"
                    },
                    {
                        id:"g2",
                        valueField:"active",
                        classNameField:"bulletClass",
                        title:"Premium",
                        type:"line",
                        valueAxis:"a2",lineColor:"#786c56",
                        lineThickness:1,
                        legendValueText:"[[value]] active referrals",
                        bullet:"round",
                        bulletSizeField:"townSize",
                        bulletBorderColor:"#02617a",
                        bulletBorderAlpha:1,
                        bulletBorderThickness:2,
                        bulletColor:"#89c4f4",
                        labelPosition:"right",
                        balloonText:"active referrals:[[value]]",
                        showBalloon:!0,animationPlayed:!0
                    }],
                chartCursor:{
                    zoomable:!1,
                    categoryBalloonDateFormat:"DD",
                    cursorAlpha:0,
                    categoryBalloonColor:"#e26a6a",
                    categoryBalloonAlpha:.8,
                    valueBalloonsEnabled:!1
                },
                legend:{
                    bulletType:"round",
                    equalWidths:!1,
                    valueWidth:120,
                    useGraphSettings:!0,
                    color:"#6c7b88"
                }
            });
    }

        
    // Sparkline
    array_numbers(18,function(e) {
        $("#sparkline_bar_today_1").sparkline(
            e,
            {
                type:"bar",
                width:"100",
                barWidth:5,
                height:"55",
                barColor:"#5c9bd1",
                negBarColor:"#e02222"});
    });

    array_numbers(18,function(e) {
        $("#sparkline_bar_today_2").sparkline(
        e,
        {
            type:"bar",
            width:"100",
            barWidth:5,
            height:"55",
            barColor:"#f36a5b",
            negBarColor:"#e02222"});
    });

    array_numbers(18,function(e) {
        $("#sparkline_bar_week_1").sparkline(
        e,
        {
            type:"bar",
            width:"100",
            barWidth:5,
            height:"55",
            barColor:"#5c9bd1",
            negBarColor:"#e02222"});
    });

    array_numbers(18,function(e) {
        $("#sparkline_bar_week_2").sparkline(
        e,
        {
            type:"bar",
            width:"100",
            barWidth:5,
            height:"55",
            barColor:"#f36a5b",
            negBarColor:"#e02222"});
    });

    array_numbers(18,function(e) {
        $("#sparkline_bar_month_1").sparkline(
        e,
        {
            type:"bar",
            width:"100",
            barWidth:5,
            height:"55",
            barColor:"#14b405",
            negBarColor:"#e02222"});
    });

    array_numbers(18,function(e) {
        $("#sparkline_bar_month_2").sparkline(
        e,
        {
            type:"bar",
            width:"100",
            barWidth:5,
            height:"55",
            barColor:"#f36a5b",
            negBarColor:"#e02222"});
    });


    array_numbers(18,function(e) {
        $("#sparkline_bar_con_today_1").sparkline(
            e,
            {
                type:"bar",
                width:"100",
                barWidth:5,
                height:"55",
                barColor:"#5c9bd1",
                negBarColor:"#e02222"});
    });

    array_numbers(18,function(e) {
        $("#sparkline_bar_con_today_2").sparkline(
        e,
        {
            type:"bar",
            width:"100",
            barWidth:5,
            height:"55",
            barColor:"#f36a5b",
            negBarColor:"#e02222"});
    });

    array_numbers(18,function(e) {
        $("#sparkline_bar_con_week_1").sparkline(
        e,
        {
            type:"bar",
            width:"100",
            barWidth:5,
            height:"55",
            barColor:"#5c9bd1",
            negBarColor:"#e02222"});
    });

    array_numbers(18,function(e) {
        $("#sparkline_bar_con_week_2").sparkline(
        e,
        {
            type:"bar",
            width:"100",
            barWidth:5,
            height:"55",
            barColor:"#f36a5b",
            negBarColor:"#e02222"});
    });

    array_numbers(18,function(e) {
        $("#sparkline_bar_con_month").sparkline(
        e,
        {
            type:"bar",
            width:"100",
            barWidth:5,
            height:"55",
            barColor:"#14b405",
            negBarColor:"#e02222"});
    });

    array_numbers(18,function(e) {
        $("#sparkline_bar_con_month6").sparkline(
        e,
        {
            type:"bar",
            width:"100",
            barWidth:5,
            height:"55",
            barColor:"#473C8B",
            negBarColor:"#e02222"});
    });

    function array_numbers(limit,callback)
    {
        var i = 0;
        var numbers = [];
        for(i; i < limit; i++) {
            var random_number = Math.ceil(Math.random() * 100);
            numbers.push(random_number);
        }
        callback(numbers);
    }

</script>
@endsection