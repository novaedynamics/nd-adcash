@component('mail::message')
# Confirm

You've requested a transfer of code to another account. Please, click the confirmation link to complete the process.

@component('mail::button', ['url' => route('confirm-transfer-codes',['code' => $transfer->token]) ])
complete transfer
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
