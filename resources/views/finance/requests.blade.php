@extends('layouts.dash')

@section('title','Finance')
@section('slogan','Payout Requests')

@section('content')

<!-- START -->
    <div class="portlet light bordered">
        <table id="payout-request" class="display" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th width="15%">Date</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Method</th>
                    <th>Phone</th>
                    <th>Address</th>
                    <th>Amount</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th width="15%">Date</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Method</th>
                    <th>Phone</th>
                    <th>Address</th>
                    <th>Amount</th>
                    <th>Status</th>
                </tr>
            </tfoot>
        </table>  
    </div>
<!-- END -->

@endsection

@section('page-plugin-css')
<!-- START -->
<link href="/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('page-plugin')
<!-- START -->
<script src="/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
@endsection

@section('page-script')
<!-- START -->
<script src="/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        var table = $('#payout-request').DataTable({
            "ajax": '/api/finance/withdraw/payout-requests',
            "order": [
                [0,'desc']
            ],
            "columns": [
            { "data": "created_at" },
            { "data": "user.name" },
            { "data": "user.lastname" },
            { "data": "method" },
            { "data": "phone" },
            { "data": "wallet" },
            { "data": "amount" },
            { "data": "status"},
        ]
        });

        $('#payout-request tbody').on( 'dblclick', 'tr', function () {
            var data = table.row($(this)).data();
            console.log(data);
        });

    });
</script>
@endsection