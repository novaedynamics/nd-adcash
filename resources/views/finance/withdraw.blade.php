@extends('layouts.dash')

@section('title','Finance')
@section('slogan','Withdraw')

@section('content')

<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-sharp">
                        <span data-counter="counterup" data-value="{{ $wallet_total }}">0</span>
                        <small class="font-green-sharp">{{ config('app.currency.us') }}</small>
                    </h3>
                    <small>WALLET BALANCE</small>
                </div>
                <div class="icon">
                    <i class="fa fa-podcast"></i>
                </div>
            </div>
            
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-red-haze">
                        <span data-counter="counterup" data-value="{{ $wallet_bonus }}">0</span>
                        <small class="font-red-haze">{{ config('app.currency.us') }}</small>
                    </h3>
                    <small>TOTAL BONUS</small>
                </div>
                <div class="icon">
                    <i class="fa fa-sign-in"></i>
                </div>
            </div>
            
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-blue-sharp">
                        <span data-counter="counterup" data-value="{{ $wallet_payout }}"></span>
                        <small class="font-blue-sharp">{{ config('app.currency.us') }}</small>
                    </h3>
                    <small>APPROVED PAYOUT</small>
                </div>
                <div class="icon">
                    <i class="fa fa-level-up"></i>
                </div>
            </div>
            
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-purple-soft">
                        <span data-counter="counterup" data-value="{{ $wallet_pending_payout }}"></span>
                        <small class="font-purple-sharp">{{ config('app.currency.us') }}</small>
                    </h3>
                    <small>PENDING PAYOUT</small>
                </div>
                <div class="icon">
                    <i class="icon-users"></i>
                </div>
            </div>
            
        </div>
    </div>
</div>

<div class="profile">
    <div class="tabbable-line tabbable-full-width">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#withdraw" data-toggle="tab" aria-expanded="true"> Withdraw </a>
            </li>
            <li class="">
                <a href="{{ route('withdraw-history') }}"> History </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="withdraw">
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption font-red-sunglo">
                            <i class="icon-settings font-red-sunglo"></i>
                            <span class="caption-subject bold uppercase"> Payout Request Form</span>
                        </div>                                    
                    </div>
                    <div class="portlet-body form">

                        @if($pending_withdrawal)
                        <div class="note note-warning">
                            <h4 class="block">Hi, {{ Auth::user()->name }}!</h4>
                            <p>You are only allowed once a week to make a payout request and we process payouts every <strong>Monday</strong> first day of the week.</p>
                        </div>
                        @endif

                        @if(!$pending_withdrawal)
                        <form role="form" method="POST" id="form-payout">
                            {{ csrf_field() }}
                            <div class="form-body">
                                <div class="form-group">
                                    <label>Amount</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-dollar"></i>
                                        </span>
                                        <select class="form-control" name="amount">
                                            @foreach($denomination as $d)
                                            <option value="{{ $d }}">{{ $currency }}{{ $d }}</option>
                                            @endforeach
                                        </select>
                                        <!-- <input class="form-control" placeholder="" type="text" name="amount"> --> </div>
                                </div>                                            
                                <div class="form-group">
                                    <label>Payment Method</label>
                                    <select class="form-control" name="payout-type">
                                        @foreach($method as $m)
                                        <option value="{{ $m }}">{{ $m }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group waddress">
                                    <label>Wallet Address</label>
                                    <input class="form-control" name="waddress" placeholder="" />
                                </div>
                                <div class="form-group wmobile">
                                    <label>Mobile</label>
                                    <input class="form-control" name="wmobile" placeholder="Make sure this is your mobile number used to verify your coins.ph account." />
                                </div>
                                <div class="form-group">
                                    <label>Message</label>
                                    <textarea class="form-control wmessage" name="wmessage"></textarea>
                                </div>

                            </div>
                            <div class="form-actions">
                                <a class="btn blue sbold btn-proceed-payout" data-toggle="modal" href="#basic"> Proceed </a>
                            </div>
                        </form>
                        @endif

                        <p><strong>Note: </strong> All withdrawals should be processed on or before the cut-off date <a href="javascript:;">{{ $cutoff_date }}</a>.</p>

                    </div>
                </div>

            </div>
            <!--tab_1_2-->
            <div class="tab-pane" id="history">
                <div class="row">
                    <div class="col-md-12">
                        <!-- <div class="note note-success">
                            <p> Please try to re-size your browser window in order to see the tables in responsive mode. </p>
                        </div> -->
                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <!-- <div class="caption">
                                    <i class="fa fa-cogs"></i>Responsive Flip Scroll Tables </div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"> </a>
                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                    <a href="javascript:;" class="reload"> </a>
                                    <a href="javascript:;" class="remove"> </a>
                                </div> -->
                            </div>
                            <div class="portlet-body flip-scroll">
                                <table class="table table-bordered table-striped table-condensed flip-content">
                                    <thead class="flip-content">
                                        <tr>
                                            <th width="33%"> Code </th>
                                            <th width="33%"> Purchased Date </th>
                                            <th width="33%"> Status </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>                                
                                        </tr>                            
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->                           
                        
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>


<!-- Modal Payout -->
<div class="modal fade in" id="basic" tabindex="-1" role="basic" aria-hidden="true" style="padding-right: 17px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Payout Request</h4>
            </div>
            <div class="modal-body">

                <div class="mt-img" style="margin-bottom: 0; text-align: center; margin-top: 30px;">
                    <img class="img-circle" src="/uploads/{{ Auth::user()->avatar }}" width="120"> </div>
                
                <center><h3>Review Details</h3></center>

                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" name="userid" value="{{ Auth::user()->id }}" />
                        <table id="user" class="table table-bordered table-striped" style="margin-top: 30px;">
                            <tbody>
                                <tr>
                                    <td style="width:30%"> Full Name </td>
                                    <td style="width:70%"> {{ Auth::user()->name }} {{ Auth::user()->middlename }} {{ Auth::user()->lastname }} </td>
                                </tr>
                                <tr>
                                    <td style="width:30%"> Method </td>
                                    <td style="width:70%"> <span class="wmethod"></span> </td>
                                </tr>
                                <tr class="wwallet">
                                    <td style="width:30%"> Address </td>
                                    <td style="width:70%" class="waddress"></td>
                                </tr>
                                <tr class="wmobile">
                                    <td style="width:30%"> Mobile </td>
                                    <td style="width:70%" class="wmobile"></td>
                                </tr>
                                <tr>
                                    <td style="width:30%"> Amount </td>
                                    <td style="width:70%"> {{config('adcash.currency.us')}}<span class="wamount"></span> </td>
                                </tr>
                                <tr>
                                    <td style="width:30%"> Message (optional) </td>
                                    <td style="width:70%" class="wmessage"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                

            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancel</button>
                <!-- <button type="button" class="btn green btn-complete-payment">submit</button> -->
                <a href="javascript:;" class="btn green submit ladda-button btn-complete-payment" data-style="expand-right" data-size="l"><span class="ladda-label">Submit</span></a>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

@endsection

@section('page-plugin-css')
<link href="/assets/pages/css/profile-2.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<!-- <link href="/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" /> -->
<link href="/assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/ladda/ladda-themeless.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />

<link href="/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<style>
    label.error { color: red; }
</style>
@endsection

@section('page-plugin')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js" type="text/javascript"></script>
<!-- <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script src="/assets/global/plugins/gmaps/gmaps.min.js" type="text/javascript"></script> -->
<!-- END PAGE LEVEL PLUGINS -->

<script src="/assets/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
<script src="/assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>

<script src="/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>

@endsection

@section('page-script')
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<script src="/assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/ladda/spin.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/ladda/ladda.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.7.1/clipboard.min.js"></script>
<script src="/assets/pages/scripts/ui-modals.min.js" type="text/javascript"></script>
<script src="/assets/pages/scripts/ui-confirmations.min.js" type="text/javascript"></script>
<script src="/js/wallet/payout.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
@endsection