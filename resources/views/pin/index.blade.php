@extends('layouts.dash')

@section('title','PIN')

@section('content')

@if (session('success'))
<div class="note note-success">
    <h4 class="block">{{session('success')}}</h4>
    <!-- <p> Duis mollis, est non commodo luctus, nisi erat mattis consectetur purus sit amet porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. </p> -->
</div>
@endif

@if (session('confirm'))
<div class="note note-warning">
    <h4 class="block">{{session('confirm')}}</h4>
    <p>A confirmation link has been dispatched to your email. Click the link to complete the request!</p>
</div>
@endif


<div class="row">
        <div class="col-md-12">
            <!-- <div class="note note-warning">
                <h4 class="block">Info!</h4>
                <p>To purchase an activation PIN</p>
            </div> -->
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">

                </div>
                <div class="portlet-body flip-scroll">
                    <table class="table table-bordered table-striped table-condensed flip-content pin">
                        <thead class="flip-content">
                            <tr>
                                <th width="15%"> Purchased Date </th>
                                <th width="15%"> PIN </th>
                                <th width="15%"> Status </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pins as $pin)
                            <tr>
                                <td>{{ $pin->created_at }}</td>
                                <td>{{ $pin->pin }}</td>
                                <td>{{ $pin->status }}</td>
                            </tr>    
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="portlet box green">
                <div class="portlet-title">
                    
                </div>
                <div class="portlet-body flip-scroll">
                    <table class="table table-bordered table-striped table-condensed flip-content pin-history">
                        <thead class="flip-content">
                            <tr>
                                <th width="20%"> Transfered Date </th>
                                <th width="80%"> Description </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($transfers as $t)
                            <tr>
                                <td>{{ $t->created_at }}</td>
                                <td>{{ $t->description }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="form-actions">
                <a href="{{ route('transfer') }}" class="btn blue">Transfer PIN</a>
                @if(in_array(Auth::user()->roles()->first()->name,['Admin','Super Admin']))
                <a href="{{ route('pin-generate') }}" class="btn blue">Generate PIN</a>
                @endif
            </div>
            <!-- END SAMPLE TABLE PORTLET-->                           
            
        </div>
    </div>

@endsection

@section('page-plugin-css')
<link href="/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
@endsection

@section('page-plugin')
<script src="/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
@endsection

@section('page-script')
<script src="/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
<script>
    $('table.pin').DataTable();
    $('table.pin-history').DataTable();
</script>
@endsection