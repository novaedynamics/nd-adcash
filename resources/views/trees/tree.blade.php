@extends('layouts.dash')

@section('title','Tree')
@section('slogan','Genealogy')

@section('content')
  <a class="btn red btn-outline sbold btn-add-head" data-toggle="modal" href="#responsive"> Add Head</a>
    <!-- <div class="note note-info">
      <h4 class="block">Tree Genealogy</h4>
      <p>Double - Click the node to add new entry</p>
    </div> -->
  <div class="row"><div id="chart-container"></div></div>

  <div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Activate Head</h4>
            </div>
            <div class="modal-body">
                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 300px;"><div class="scroller" style="height: 300px; overflow: hidden; width: auto;" data-always-visible="1" data-rail-visible1="1" data-initialized="1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="timeline-item">
                                <div class="timeline-badge" style="text-align: center;">
                                    <img class="img-circle add-node-avatar" src="/uploads/{{ Auth::user()->avatar }}" 
                                      style="margin: auto; width: 120px;"> 
                                </div>   
                            </div>                                                               
                        </div>
                        <div class="col-md-12">
                            <span class="account_name" style="text-align: center; display: block;">
                              {{ Auth::user()->name }}</span>
                            <span class="head_name" style="text-align: center; display: block;">
                            <span class="wallet"></span>
                        </div>
                        <div class="col-md-12">                          
                          <div class="portlet-body form">
                              <form role="form" class="form-add-node">                                  
                                  <div class="form-body">
                                      <div class="form-group">
                                          <label>Your Placement Email</label>
                                         <div class="input-group">
                                            <div class="referral-link">
                                                <input class="form-control" name="node-name" type="text" id="input-node-name" ></div>
                                                <span class="input-group-btn">
                                                <a href="javascript:;" onclick="copy_referral()" class="btn btn-success" type="button">
                                                    Copy To Clipboard</a>
                                            </span>
                                        </div>
                                        <p style="font-size: 11px;"><strong>Note:</strong> Use this "Placement Email" to place new activated account below it. Give this to your downline</p>
                                      </div>
                                  </div>                                       
                              </form>
                          </div>

                        </div>

                    </div><div class="slimScrollBar" style="background: rgb(187, 187, 187) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 300px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(234, 234, 234) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div></div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                <!-- <button type="button" class="btn green btn-add-node"></button> -->
            </div>
        </div>
    </div>
</div>
@endsection

<!--  -->

@section('page-plugin-css')
<link rel="stylesheet" href="https://rawgit.com/dabeng/OrgChart/master/demo/css/font-awesome.min.css">
<link rel="stylesheet" href="https://rawgit.com/dabeng/OrgChart/master/demo/css/jquery.orgchart.css">
<link rel="stylesheet" href="https://rawgit.com/dabeng/OrgChart/master/demo/css/style.css">
<style type="text/css">
  .btn-add-head { display: none; }
  /*.orgchart .node .edge { display: none; }*/
  #chart-container { background-color: #fff; height: 680px; margin-bottom: 30px }
  .orgchart { background: #fff; }
  .orgchart.view-state .edge { display: none; }
  .orgchart .node { width: 150px; }
  .orgchart .node .title .symbol { margin-top: 1px; }
  #edit-panel {
    position: relative;
    left: 10px;
    width: calc(100% - 40px);
    border-radius: 4px;
    float: left;
    margin-top: 10px;
    padding: 10px;
    color: #fff;
    background-color: #449d44;
  }
  #edit-panel .btn-inputs { font-size: 24px; }
  #edit-panel.view-state>:not(#chart-state-panel) { display: none; }
  #edit-panel label { font-weight: bold; }
  #edit-panel.edit-parent-node .selected-node-group { display: none; }
  #chart-state-panel, #selected-node, #btn-remove-input { margin-right: 20px; }
  #edit-panel button {
    color: #333;
    background-color: #fff;
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
  }
  #edit-panel.edit-parent-node button:not(#btn-add-nodes) { display: none; }
  #edit-panel button:hover,.edit-panel button:focus,.edit-panel button:active {
    border-color: #eea236;
    box-shadow:  0 0 10px #eea236;
  }
  #new-nodelist {
    display: inline-block;
    list-style:none;
    margin-top: -2px;
    padding: 0;
    vertical-align: text-top;
  }
  #new-nodelist>* { padding-bottom: 4px; }
  .btn-inputs { vertical-align: sub; }
  #edit-panel.edit-parent-node .btn-inputs { display: none; }
  .btn-inputs:hover { text-shadow: 0 0 4px #fff; }
  .radio-panel input[type='radio'] { display: inline-block;height: 24px;width: 24px;vertical-align: top; }
  #edit-panel.view-state .radio-panel input[type='radio']+label { vertical-align: -webkit-baseline-middle; }
  #btn-add-nodes { margin-left: 20px; }


    .orgchart .second-menu-icon {
      transition: opacity .5s;
      opacity: 0;
      right: -5px;
      top: -5px;
      z-index: 2;
      color: rgba(68, 157, 68, 0.5);
      font-size: 18px;
      position: absolute;
    }
    .orgchart .second-menu-icon:hover { color: #449d44; }
    .orgchart .node:hover .second-menu-icon { opacity: 1; }
    .orgchart .node .second-menu {
      /*display: none;*/
      position: absolute;
      top: -70px;
      right: 40px;
      border-radius: 35px;
      box-shadow: 0 0 10px 1px #999;
      background-color: #fff;
      z-index: 1;
    }
    .orgchart .node .second-menu .avatar {
      width: 60px;
      height: 60px;
      border-radius: 30px;
      float: left;
      margin: 5px;
    }

    .orgchart .lines .downLine {
      height: 70px !important;
    }

    .orgchart p.earnings { margin: 0px; }

</style>
@endsection

<!--  -->

@section('page-plugin')
<!-- <script type="text/javascript" src="https://rawgit.com/dabeng/OrgChart/master/demo/js/jquery.min.js"></script> -->
<script type="text/javascript" src="https://rawgit.com/dabeng/OrgChart/master/demo/js/html2canvas.min.js"></script>
<script type="text/javascript" src="https://rawgit.com/dabeng/OrgChart/master/demo/js/jquery.orgchart.js"></script>
@endsection

<!--  -->

@section('page-script')

<script type="text/javascript">
    $(function() {

      var getId = function() {
        return (new Date().getTime()) * 1000 + Math.floor(Math.random() * 1001);
      };

      $.get('/api/tree',function(e) {

          datascource = e.root;

          $.get('/api/users/avatars',function(e) {
            App.avatars = e;
            ochart(datascource, App.avatars);
          });
          
      });

      

      var ochart = function(data, avatars) {
        var oc = $('#chart-container').orgchart({
          'data' : data,
          'nodeContent': 'title',
          'pan': true,
          'zoom': true,
          'visibleLevel': 5,
          // 'direction': 'l2r',
          'initCompleted': function ($chart) {
            var $container = $('#chart-container');
            // $container.scrollTop(-1000);
            // $container.scrollTop(($container[0].scrollHeight - $container.height())/2);
            $container.scrollLeft(($container[0].scrollWidth - $container.width())/2);
            // $('#chart-container .orgchart').css('top',"10%").css("position","absolute");
            // $('#chart-container .orgchart').css('left',"40%").css("position","absolute");
            // $container.scrollTop(($container[0].scrollWidth - $container.height())/2);
            // $container.scrollLeft(500);

            // console.log(($container[0].scrollWidth - $container.height())/2);

          },
          'createNode': function($node, d) {
            $node[0].id = getId();            

            avatars.map(function(i) {

              if(i.id == d.user_id) {
                  $node.find('.title').text(i.name);
                  var secondMenuIcon = $('<i>', {
                    'class': 'fa fa-info-circle second-menu-icon',
                    click: function() {
                      $(this).siblings('.second-menu').toggle();
                    }
                  });
                  var secondMenu = `<div class="second-menu img-circle"><img class="avatar img-circle" src="/uploads/`+ i.avatar +`"></div>`;
                  $node.append(secondMenu);
                  $node.find('.content').append(`<p class="earnings">`+i.earnings+`</p>`);

              }
            });




            $node.on('dblclick', function(event) {

              if (!$(event.target).is('.edge')) {
                  console.log(d.user_id);

                if (typeof d.children == "undefined" || d.children.length < 2) {

                      avatars.map(function(i) {
                        if(i.id == d.user_id) {
                           jQuery('.account_name').html(i.name);
                           jQuery('.add-node-avatar').attr('src','/uploads/' + i.avatar);
                           jQuery('.btn-add-head').trigger('click');
                           jQuery('input[name="node-name"]').val(i.email);
                        }
                      });

                   

                } else {
                  alert('You cannot use this head any longer, nodes are already in used.');
                }

              }

            });

          }
        });

        oc.$chartContainer.on('touchmove', function(event) {
          event.preventDefault();
        });

      }

  });

  </script>
  <script src="/assets/pages/scripts/ui-modals.min.js" type="text/javascript"></script>
  <script src="/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
  <script>
    function copy_referral() {
      /* Get the text field */
      var copyText = document.getElementById("input-node-name");

      /* Select the text field */
      copyText.select();

      /* Copy the text inside the text field */
      document.execCommand("Copy");

      /* Alert the copied text */
      alert("Copied the text: " + copyText.value);
      // alert('dfsdfs');
    } 
</script>
@endsection