@extends('layouts.dash')

@section('title','Profile')

@section('content')

<div class="profile">
        <div class="tabbable-line tabbable-full-width">
            <ul class="nav nav-tabs">
                <li>
                    <a href="#tab_1_1" data-toggle="tab"> Account</a>
                </li>
            </ul>
            <div class="tab-content">                
                <!--tab_1_2-->
                <div class="tab-pane active" id="tab_1_1">
                    <div class="row profile-account">
                        <div class="col-md-3">
                            <ul class="ver-inline-menu tabbable margin-bottom-10">
                                <li class="active">
                                    <a data-toggle="tab" href="#change-profile">
                                        <i class="fa fa-cog"></i> Personal info </a>
                                    <span class="after"> </span>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#change-avatar">
                                        <i class="fa fa-picture-o"></i> Change Avatar </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#change-password">
                                        <i class="fa fa-lock"></i> Change Password </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-9">
                            <div class="tab-content">
                                <div id="change-profile" class="tab-pane active">
                                    <form role="form" action="#" id="form-profile">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label class="control-label">First Name</label>
                                            <input type="text" placeholder="John" class="form-control" name="name" value="{{ $user->name }}" /> </div>
                                        <div class="form-group">
                                            <label class="control-label">Last Name</label>
                                            <input type="text" placeholder="Doe" class="form-control" name="lastname" value="{{ $user->lastname }}" /> </div>
                                        <div class="form-group">
                                            <label class="control-label">Middle Name</label>
                                            <input type="text" placeholder="" class="form-control" name="middlename" value="{{ $user->middlename }}" /> </div>
                                        <div class="form-group">
                                            <label class="control-label">Mobile Number</label>
                                            <input type="text" placeholder="+1 646 580 DEMO (6284)" class="form-control" name="mobile" value="{{ $user->mobile }}" /> </div>
                                        <div class="form-group">
                                            <label class="control-label">Phone Number</label>
                                            <input type="text" placeholder="+1 646 580 DEMO (6284)" class="form-control" name="phone" value="{{ $user->phone }}" /> </div>
                                        <div class="form-group">
                                            <label class="control-label">Address</label>
                                            <input type="text" class="form-control" name="address" value="{{ $user->address }}" /> </div>
                                        <div class="form-group">
                                            <label class="control-label">City</label>
                                            <input type="text" class="form-control" name="city" value="{{ $user->city }}" /> </div>
                                        <div class="form-group">
                                            <label class="control-label">Postal</label>
                                            <input type="text" class="form-control" name="postal" value="{{ $user->postal }}" /> </div>
                                        <div class="form-group">
                                            <label class="control-label">Province</label>
                                            <input type="text" class="form-control" name="province" value="{{ $user->province }}" /> </div>
                                        <div class="form-group">
                                            <label class="control-label">Country</label>
                                            <input type="text" class="form-control" name="country" value="{{ $user->country }}" /> </div>
                                        <div class="form-group">
                                            <label class="control-label">About</label>
                                            <textarea class="form-control" rows="3" placeholder="We are KeenThemes!!!" name="about">{{ $user->about }}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Facebook Url</label>
                                            <input type="text" placeholder="https://www.facebook.com/{{config('app.facebook')}}" class="form-control" name="facebook_url" value="{{ $user->facebook_url }}" /> </div>
                                        <div class="form-group">
                                            <label class="control-label">Twitter Url</label>
                                            <input type="text" placeholder="https://www.twitter.com/{{config('app.twitter')}}" class="form-control" name="twitter_url" value="{{ $user->twitter_url }}" /> </div>
                                        <div class="margiv-top-10">
                                            <!-- <a href="javascript:;" class="btn green btn-profile-save"> Save Changes </a> -->
                                            <a href="javascript:;" class="btn green submit ladda-button btn-profile-save" data-style="expand-right" data-size="l"><span class="ladda-label">Submit</span></a>
                                            <a href="javascript:;" class="btn default"> Cancel </a>
                                        </div>
                                    </form>
                                </div>
                                <div id="change-avatar" class="tab-pane">
                                    <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                                        </p>
                                    <form action="#" role="form" id="form-change-avatar" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <?php if(Auth::user()->avatar):?>
                                                    <img src="/uploads/<?php echo Auth::user()->avatar;?>" alt="">  
                                                <?php else:?>
                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> 
                                                <?php endif;?>
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                <div>
                                                    <span class="btn default btn-file">
                                                        <span class="fileinput-new"> Select image </span>
                                                        <span class="fileinput-exists"> Change </span>
                                                        <input name="avatar-image" type="file" required> </span>
                                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>
                                            <div class="clearfix margin-top-10">
                                                <span class="label label-danger">NOTE! </span>
                                                <span> Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                            </div>
                                        </div>
                                        <div class="margin-top-10">
                                            <!-- <a href="javascript:;" class="btn green submit"> Submit </a> -->
                                            <a href="javascript:;" class="btn green submit ladda-button" data-style="expand-right" data-size="l"><span class="ladda-label">Submit</span></a>
                                            <a href="javascript:;" class="btn default"> Cancel </a>
                                        </div>
                                    </form>
                                </div>
                                <div id="change-password" class="tab-pane">
                                    <form action="#" id="form-change-password">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <label class="control-label">Current Password</label>
                                            <input type="password" class="form-control" name="current-password" /> </div>
                                        <div class="form-group">
                                            <label class="control-label">New Password</label>
                                            <input type="password" class="form-control" name="new-password" /> </div>
                                        <div class="form-group">
                                            <label class="control-label">Re-type New Password</label>
                                            <input type="password" class="form-control" name="confirm-password" /> </div>
                                        <div class="margin-top-10">
                                            <!-- <a href="javascript:;" class="btn green btn-change-password"> Change Password </a> -->
                                            <a href="javascript:;" class="btn green submit ladda-button btn-change-password" data-style="expand-right" data-size="l"><span class="ladda-label">Submit</span></a>
                                            <a href="javascript:;" class="btn default"> Cancel </a>
                                        </div>
                                    </form>
                                </div>                                
                            </div>
                        </div>
                        <!--end col-md-9-->
                    </div>
                </div>
                <!--end tab-pane-->
                
            </div>
        </div>
    </div>
@endsection

@section('page-plugin-css')
<link href="/assets/pages/css/profile-2.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<!-- <link href="/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" /> -->
<link href="/assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/ladda/ladda-themeless.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
<style>
    label.error { color: red; }
</style>
@endsection

@section('page-script')
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<!-- <script src="/assets/pages/scripts/profile.min.js" type="text/javascript"></script>
<script src="/assets/pages/scripts/timeline.min.js" type="text/javascript"></script> -->
    <script src="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
    <script src="/assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/ladda/spin.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/ladda/ladda.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script>
<script src="/js/users/avatar.js" type="text/javascript"></script>
<script src="/js/users/user.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
@endsection

@section('page-plugin')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script src="/assets/global/plugins/gmaps/gmaps.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
@endsection