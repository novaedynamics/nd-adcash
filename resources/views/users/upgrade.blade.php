@extends('layouts.dash')

@section('title','Profile')
@section('slogan','Upgrade Account')

@section('content')

@if(session('danger'))
<div class="note note-danger">
    <h4 class="block">{{ session('danger') }}</h4>
</div>
@endif

@if(count($codes) > 0 && count($pins) > 0)
<div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <!-- <i class="fa fa-arrow-up"></i> -->
                        <span class="caption-subject bold uppercase"> Upgrade Account</span>
                    </div>                    
                </div>
                <div class="portlet-body form">
                    <form role="form" method="POST" action="{{ route('account-upgrade') }}">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <label>Placement Email</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-id-card"></i>
                                    </span>
                                    <!-- <input name="email" class="form-control tt-input" autocomplete="off" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: transparent;" type="text"> -->
                                    <input class="form-control" 
                                        autocomplete="off" 
                                        placeholder="" type="text" name="email"> </div>
                            </div>
                            <div class="form-group">
                                <label>Activation Code</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-code"></i>
                                    </span>
                                    <select class="form-control" placeholder="Enter activation code" type="text" name="activation-code">
                                        @foreach($codes as $c)
                                        <option>{{ $c->code }}</option>
                                        @endforeach
                                    </select>
                                    <!-- <input class="form-control" placeholder="Enter activation code" type="text" name="activation-code"> --> </div>
                            </div>
                            <div class="form-group">
                                <label>Pin</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-key"></i>
                                    </span>
                                    <select class="form-control" placeholder="Enter activation code" type="text" name="pin">
                                        @foreach($pins as $p)
                                        <option>{{ $p->pin }}</option>
                                        @endforeach
                                    </select>
                                    <!-- <input class="form-control" placeholder="Enter PIN for your activation code" type="password" name="pin"> --> </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn blue">Submit</button>
                            <button type="button" class="btn default">Clear</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->
            
        </div>

</div>
@else

<div class="note note-info">
    <h4 class="block">Required!</h4>
    <p>You need both of the <strong>activation code</strong> and <strong>PIN</strong> to make this action.</p>
</div>

@endif

@endsection

@section('page-plugin-css')
<link href="/assets/global/plugins/typeahead/typeahead.css" rel="stylesheet" type="text/css" />
@endsection

@section('page-script')
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="/assets/pages/scripts/components-typeahead.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
@endsection

@section('page-plugin')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="/assets/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
@endsection
