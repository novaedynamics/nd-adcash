@extends('layouts.dash')

@section('title','Finance');
@section('slogan','Withdraw');

@section('content')

<div class="profile">
    <div class="tabbable-line tabbable-full-width">
        <ul class="nav nav-tabs">
            <li class="">
                <a href="{{ route('withdraw') }}"> Withdraw </a>
            </li>
            <li class="active">
                <a href="{{ route('withdraw-history') }}"> History </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="withdraw">
                
                <div class="portlet box green">
                    <div class="portlet-title">
                        <!-- <div class="caption">
                            <i class="fa fa-cogs"></i>Responsive Flip Scroll Tables </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"> </a>
                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="remove"> </a>
                        </div> -->
                    </div>
                    <div class="portlet-body flip-scroll">
                        <table class="table table-bordered table-striped table-condensed flip-content history">
                            <thead class="flip-content">
                                <tr>
                                    <th width="20%"> Date </th>
                                    <th width="20%"> Name </th>
                                    <th width="20%"> Method </th>
                                    <th width="20%"> Amount </th>
                                    <th width="15%"> Status </th>
                                    <th width="5%"> Note </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($payouts as $p)
                                <tr>
                                    <td>{{ $p->created_at }}</td>
                                    <td>{{ $p->user->name }}</td>
                                    <td>{{ $p->method }}</td>
                                    <td>{{ config('adcash.currency.us') }}{{ $p->amount }}</td>
                                    <td>{{ $p->status }}</td>
                                    <td><center><a href="javascript:;"><i class="fa fa-sticky-note"></i></a></center></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>                      
        </div>
    </div>
</div>

@endsection

@section('page-plugin-css')
<link href="/assets/pages/css/profile-2.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<!-- <link href="/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" /> -->
<link href="/assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/ladda/ladda-themeless.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />

<link href="/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
<style>
    label.error { color: red; }
</style>
@endsection

@section('page-plugin')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js" type="text/javascript"></script>
<!-- <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script src="/assets/global/plugins/gmaps/gmaps.min.js" type="text/javascript"></script> -->
<!-- END PAGE LEVEL PLUGINS -->

<script src="/assets/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
<script src="/assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

@endsection

@section('page-script')
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<script src="/assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/ladda/spin.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/ladda/ladda.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.7.1/clipboard.min.js"></script>
<script src="/assets/pages/scripts/ui-modals.min.js" type="text/javascript"></script>
<script src="/assets/pages/scripts/ui-confirmations.min.js" type="text/javascript"></script>
<script src="/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
<script src="/js/wallet/payout.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<script>
    $('table.history').DataTable({
        
    });
</script>
@endsection